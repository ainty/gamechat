﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace Definitions
{
    internal static class FieldsSerializer<T> where T : class, new()
    {
        private const int IntSize = sizeof(int);
        private const int DoubleSize = sizeof(double);
        private const int DecimalSize = sizeof(decimal);
        private static int[] _fieldCodes;
        private static Dictionary<int, FieldInfo> _fieldDict;

        public static byte[] Serialize(T package)
        {
            var result = new List<byte>();

            foreach (var fieldName in _fieldCodes)
            {
                var field = _fieldDict[fieldName.GetHashCode()];

                if (IsString(field))
                {
                    var fieldData = Get<string>(package, fieldName.GetHashCode());
                    result.AddRange(fieldData != null
                        ? BitConverter.GetBytes(fieldData.Length)
                        : BitConverter.GetBytes(0));
                }
                else if (IsInteger(field))
                {
                    result.AddRange(BitConverter.GetBytes(IntSize));
                }
                else if (IsDouble(field))
                {
                    result.AddRange(BitConverter.GetBytes(DoubleSize));
                }
                else if (IsDecimal(field))
                {
                    result.AddRange(BitConverter.GetBytes(DecimalSize));
                }
            }

            foreach (var fieldName in _fieldCodes)
            {
                var field = _fieldDict[fieldName];

                if (IsString(field))
                {
                    var fieldData = Get<string>(package, fieldName);
                    if (fieldData != null)
                    {
                        result.AddRange(Encoding.UTF8.GetBytes(Get<string>(package, fieldName)));
                    }
                }
                else if (IsInteger(field))
                {
                    result.AddRange(BitConverter.GetBytes(Get<int>(package, fieldName)));
                }
                else if (IsDouble(field))
                {
                    result.AddRange(BitConverter.GetBytes(Get<double>(package, fieldName)));
                }
                else if (IsDecimal(field))
                {
                    result.AddRange(BitConverterExtension.GetBytes(Get<decimal>(package, fieldName)));
                }
            }

            return result.ToArray();
        }

        public static T Deserialize(byte[] data)
        {
            var result = new T();

            var fieldLengths = new int[_fieldCodes.Length];

            for (var i = 0; i < _fieldCodes.Length; i++)
            {
                var fieldName = _fieldCodes[i];
                var field = _fieldDict[fieldName];

                fieldLengths[i] = GetFieldValueLength(data, IntSize * i, field);
            }

            var initialOffset = IntSize * _fieldCodes.Length;
            var offset = 0;

            for (var i = 0; i < _fieldCodes.Length; i++)
            {
                var fieldName = _fieldCodes[i];
                var field = _fieldDict[fieldName];
                var fieldLength = fieldLengths[i];

                if (IsString(field))
                {
                    var fieldValue = GetStringFieldValue(data, initialOffset + offset, fieldLength);

                    Set(result, fieldName, fieldValue);
                }
                else if (IsInteger(field))
                {
                    var fieldValue = GetIntFieldValue(data, initialOffset, offset);

                    Set(result, fieldName, fieldValue);
                }
                else if (IsDouble(field))
                {
                    var fieldValue = GetDoubleFieldValue(data, initialOffset, offset);

                    Set(result, fieldName, fieldValue);
                }
                else if (IsDecimal(field))
                {
                    var fieldValue = GetDecimalFieldValue(data, initialOffset, offset);

                    Set(result, fieldName, fieldValue);
                }

                offset += fieldLength;
            }

            return result;
        }

        public static void Initialize()
        {
            if (_fieldCodes != null && _fieldDict != null) return;

            var fields = typeof(T).GetFields();

            var orderedFieldNames = new SortedList<string, int>();
            foreach (var field in fields)
            {
                orderedFieldNames.Add(field.Name, field.Name.GetHashCode());
            }

            _fieldCodes = new int[orderedFieldNames.Count];
            var i = 0;
            foreach (var a in orderedFieldNames)
            {
                _fieldCodes[i] = a.Value;
                i++;
            }

            _fieldDict = new Dictionary<int, FieldInfo>();
            foreach (var field in fields)
            {
                _fieldDict.Add(orderedFieldNames[field.Name], field);
            }
        }

        private static TV Get<TV>(T package, int fieldName)
        {
            return (TV)_fieldDict[fieldName].GetValue(package);
        }

        private static void Set(T package, int fieldName, object value)
        {
            _fieldDict[fieldName].SetValue(package, value);
        }

        private static bool IsInteger(FieldInfo field)
        {
            return field.FieldType.IsEnum || field.FieldType == typeof(int);
        }

        private static bool IsDouble(FieldInfo field)
        {
            return field.FieldType == typeof(double);
        }

        private static bool IsDecimal(FieldInfo field)
        {
            return field.FieldType == typeof(decimal);
        }

        private static bool IsString(FieldInfo field)
        {
            return field.FieldType == typeof(string);
        }

        private static int GetIntFieldValue(byte[] data, int initialOffset, int offset)
        {
            return BitConverter.ToInt32(data, initialOffset + offset);
        }

        private static double GetDoubleFieldValue(byte[] data, int initialOffset, int offset)
        {
            return BitConverter.ToDouble(data, initialOffset + offset);
        }

        private static decimal GetDecimalFieldValue(byte[] data, int initialOffset, int offset)
        {
            return BitConverterExtension.ToDecimal(data, initialOffset + offset);
        }

        private static string GetStringFieldValue(byte[] data, int offset, int fieldValueLength)
        {
            return fieldValueLength > 0
                ? Encoding.UTF8.GetString(data, offset, fieldValueLength)
                : null;
        }

        private static int GetFieldValueLength(byte[] data, int offset, FieldInfo field)
        {
            if (IsString(field))
            {
                return BitConverter.ToInt32(data, offset);
            }

            if (IsInteger(field))
            {
                return IntSize;
            }

            if (IsDouble(field))
            {
                return DoubleSize;
            }

            if (IsDecimal(field))
            {
                return DecimalSize;
            }

            throw new ArgumentOutOfRangeException(field.FieldType.Name);
        }
    }

    public static class BitConverterExtension
    {
        public static byte[] GetBytes(decimal dec)
        {
            //Load four 32 bit integers from the Decimal.GetBits function
            int[] bits = decimal.GetBits(dec);
            //Create a temporary list to hold the bytes
            var bytes = new List<byte>();
            //iterate each 32 bit integer
            foreach (int i in bits)
            {
                //add the bytes of the current 32bit integer
                //to the bytes list
                bytes.AddRange(BitConverter.GetBytes(i));
            }
            //return the bytes list as an array
            return bytes.ToArray();
        }

        public static decimal ToDecimal(byte[] bytes, int offset)
        {
            //make an array to convert back to int32's
            int[] bits = new int[4];
            for (int i = 0; i <= 15; i += 4)
            {
                //convert every 4 bytes into an int32
                bits[i / 4] = BitConverter.ToInt32(bytes, offset + i);
            }
            //Use the decimal's new constructor to
            //create an instance of decimal
            return new decimal(bits);
        }
    }
}
