﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Definitions
{
    public static class TypeHelper
    {
        public static List<Type> GetImplementationsOf<T>(Predicate<Type> predicate)
        {
            var list = new List<Type>();

            foreach (var assembly in GetRelatedAssemblies<T>())
            {
                var types = new Type[0];
                try
                {
                    types = assembly.GetExportedTypes();
                }
                catch (TypeLoadException)
                {
                }

                foreach (var type in types)
                {
                    if (!type.IsAbstract && typeof (T).IsAssignableFrom(type))
                    {
                        if (predicate(type))
                        {
                            list.Add(type);
                        }
                    }
                }
            }

            return list;
        }

        public static List<Type> GetImplementationsOf<T>()
        {
            return GetImplementationsOf<T>(t => true);
        }

        private static List<Assembly> GetRelatedAssemblies<T>()
        {
            var assemblies = new List<Assembly>();

            var location = Path.GetDirectoryName(typeof (T).Assembly.Location);
            if (location != null && Directory.Exists(location))
            {
                foreach (var file in Directory.GetFiles(location))
                {
                    if (file.EndsWith(".dll"))
                    {
                        try
                        {
                            assemblies.Add(Assembly.LoadFile(file));
                        }
                        catch (BadImageFormatException)
                        {
                        }
                    }
                }
            }

            return assemblies;
        }

        public static T Create<T>(Type type, params object[] args)
        {
            return (T) Activator.CreateInstance(type, args);
        }

        public static bool Has<TA>(Type type) where TA : Attribute
        {
            return Attribute.IsDefined(type, typeof (TA));
        }

        public static TA Get<TA>(Type type) where TA : Attribute
        {
            return (TA) Attribute.GetCustomAttribute(type, typeof (TA));
        }

        public static T Get<T>(Predicate<Type> typeSelector, params object[] args)
        {
            var types = GetImplementationsOf<T>(typeSelector);
            var type = types.Find(t => true);
            var instance = Create<T>(type, args);

            return instance;
        }

        public static Predicate<T> Combine<T>(params Predicate<T>[] predicates)
        {
            switch (predicates.Length)
            {
                case 0:
                    return t => true;
                default:
                    var tail = Tail(predicates);
                    return t => predicates[0](t) && Combine(tail)(t);
            }
        }

        public static T[] Tail<T>(T[] original)
        {
            var tail = new T[original.Length - 1];
            Array.Copy(original, 1, tail, 0, original.Length - 1);
            return tail;
        }

        public static Predicate<Type> Is<T>()
        {
            return t => typeof(T).IsAssignableFrom(t);
        }

        public static Predicate<Type> With<TA>(string mode) where TA : WayAttribute
        {
            return t => Get<TA>(t).Mode == mode;
        }
    }
} ;