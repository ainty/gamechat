﻿using System;

namespace Definitions
{
    public class ProtocolAttribute : Attribute
    {
        public readonly string Protocol;

        public ProtocolAttribute(string protocol)
        {
            Protocol = protocol;
        }
    }
}
