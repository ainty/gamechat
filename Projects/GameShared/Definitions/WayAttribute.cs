﻿using System;

namespace Definitions
{
    public class WayAttribute : Attribute
    {
        public readonly string Mode;

        public WayAttribute(string mode)
        {
            Mode = mode;
        }
    }
}
