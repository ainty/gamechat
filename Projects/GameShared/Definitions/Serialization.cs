﻿namespace Definitions
{
    public class Serialization<T> where T : class, new()
    {
        static Serialization()
        {
            FieldsSerializer<T>.Initialize();
        }

        public byte[] GetBytes(T package)
        {
            return FieldsSerializer<T>.Serialize(package);
        }

        public T GetObject(byte[] bytes)
        {
            return FieldsSerializer<T>.Deserialize(bytes);
        }
    }
}
