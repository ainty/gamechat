﻿
using System.Net.Sockets;
using ConnectionSchemes;

namespace SocketProviders
{
    public class CustomSocketProvider : SocketProvider
    {
        public CustomSocketProvider(SocketScheme scheme) : base(scheme)
        {
        }

        public override Socket CreateSocket()
        {
            return new Socket(Scheme.AddressFamily, Scheme.SocketType, Scheme.ProtocolType);
        }
    }
}
