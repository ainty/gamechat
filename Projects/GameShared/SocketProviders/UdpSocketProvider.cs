﻿using System.Net.Sockets;
using ConnectionSchemes;
using Definitions;

namespace SocketProviders
{
    [Protocol("udp")]
    public class UdpSocketProvider : SocketProvider
    {
        public UdpSocketProvider(SocketScheme scheme) : base(scheme)
        {
        }

        public override Socket CreateSocket()
        {
            return new Socket(Scheme.AddressFamily, SocketType.Dgram, ProtocolType.Udp);
        }
    }
}
