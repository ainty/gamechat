﻿using System.Net;
using System.Net.Sockets;
using ConnectionSchemes;

namespace SocketProviders
{
    public class IpEndPointProvider : EndPointProvider
    {
        public IpEndPointProvider(EndPointScheme scheme) : base (scheme)
        {
        }

        public override EndPoint CreateEndpoint()
        {
            var ipAddress = Scheme.IpAddress.AddressFamily == AddressFamily.InterNetwork
                ? IPAddress.Loopback
                : IPAddress.IPv6Loopback;

            return new IPEndPoint(ipAddress, Scheme.Port);
        }

        public override EndPoint CreateEndpointForIncoming()
        {
            var ipAddress = Scheme.IpAddress.AddressFamily == AddressFamily.InterNetwork
                ? IPAddress.Any
                : IPAddress.IPv6Any;

            return new IPEndPoint(ipAddress, Scheme.Port);
        }
    }
}
