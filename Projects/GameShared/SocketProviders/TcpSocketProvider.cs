﻿using System.Net.Sockets;
using ConnectionSchemes;
using Definitions;

namespace SocketProviders
{
    [Protocol("tcp")]
    public class TcpSocketProvider : SocketProvider
    {
        public TcpSocketProvider(SocketScheme scheme) : base(scheme)
        {
        }

        public override Socket CreateSocket()
        {
            return new Socket(Scheme.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
        }
    }
}
