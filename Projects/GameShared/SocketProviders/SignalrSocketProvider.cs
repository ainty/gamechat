﻿using System;
using System.Net.Sockets;
using ConnectionSchemes;
using Definitions;

namespace SocketProviders
{
    [Protocol("signalr")]
    public class SignalrSocketProvider : SocketProvider
    {
        public SignalrSocketProvider(SocketScheme scheme) : base(scheme)
        {
        }

        public override Socket CreateSocket()
        {
            throw new NotSupportedException();
        }
    }
}
