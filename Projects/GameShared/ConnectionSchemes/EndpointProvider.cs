﻿using System.Net;

namespace ConnectionSchemes
{
    public abstract class EndPointProvider
    {
        protected EndPointScheme Scheme { get; }

        protected EndPointProvider(EndPointScheme scheme)
        {
            Scheme = scheme;
        }

        public abstract EndPoint CreateEndpoint();

        public abstract EndPoint CreateEndpointForIncoming();
    }
}
