﻿using System.Net.Sockets;

namespace ConnectionSchemes
{
    public readonly struct SocketScheme
    {
        public readonly SocketType SocketType;
        public readonly AddressFamily AddressFamily;
        public readonly ProtocolType ProtocolType;

        public SocketScheme(SocketType socketType, AddressFamily addressFamily, ProtocolType protocolType)
        {
            SocketType = socketType;
            AddressFamily = addressFamily;
            ProtocolType = protocolType;
        }

        private SocketScheme(AddressFamily addressFamily): this(SocketType.Unknown, addressFamily, ProtocolType.Unknown)
        {
        }

        public SocketScheme(EndPointScheme endPointScheme) : this(endPointScheme.IpAddress.AddressFamily)
        {
        }
    }
}