using System.Net;

namespace ConnectionSchemes
{
    public readonly struct EndPointScheme
    {
        public readonly IPAddress IpAddress;
        public readonly int Port;

        public EndPointScheme(IPAddress ipAddress, int port)
        {
            IpAddress = ipAddress;
            Port = port;
        }
    }
}