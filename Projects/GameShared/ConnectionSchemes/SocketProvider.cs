﻿using System.Net.Sockets;

namespace ConnectionSchemes
{
    public abstract class SocketProvider
    {
        protected SocketScheme Scheme { get; }

        protected SocketProvider(SocketScheme scheme)
        {
            Scheme = scheme;
        }

        public abstract Socket CreateSocket();
    }
}
