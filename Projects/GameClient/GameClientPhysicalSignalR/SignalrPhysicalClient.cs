﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using ConnectionSchemes;
using Definitions;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;

namespace GameClientPhysical
{
    [Way("tap"), Protocol("signalr")]
    public sealed class SignalRPhysicalClient : PhysicalClient
    {
        private IHost _host;
        private readonly int _port;
        private readonly IPAddress _address;
        private bool _isConnected;

        public SignalRPhysicalClient(SocketProvider socketProvider, EndPointProvider gatewayEndPointProvider) : base(socketProvider, gatewayEndPointProvider)
        {
            var ep = gatewayEndPointProvider.CreateEndpoint() as IPEndPoint;
            _port = ep?.Port ?? 5000;
            _address = ep?.Address ?? IPAddress.Loopback;
        }

        public override void Connect()
        {
            if (_isConnected) return;
            _isConnected = true;

            try
            {
                _host = new HostBuilder()
                    .ConfigureServices((services) =>
                    {
                        services.TryAddSingleton(sp => this);
                        services.TryAddSingleton(sp => new ChatHubClient(sp.GetService<SignalRPhysicalClient>()));
                        services.AddHostedService(sp => sp.GetService<ChatHubClient>());
                    })
                    .Build();

                Task.Run(async () => { await _host.StartAsync(); })
                    .ContinueWith(t =>
                    {
                        var ex = t.Exception?.GetBaseException() ?? t.Exception;
                        RaiseExceptionEvent(ex);
                    }, TaskContinuationOptions.OnlyOnFaulted);
            }
            catch (AggregateException ex)
            {
                foreach (var e in ex.InnerExceptions)
                {
                    RaiseExceptionEvent(e);
                }
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }

        public override void Send(byte[] data, Action<string> callback = null)
        {
            var hubClient = _host?.Services.GetRequiredService<ChatHubClient>();

            Task.Run(() => { hubClient?.RegisterMessage(data, callback); })
                .ContinueWith(t =>
                {
                    var ex = t.Exception?.GetBaseException() ?? t.Exception;
                    RaiseExceptionEvent(ex);
                }, TaskContinuationOptions.OnlyOnFaulted);
        }

        public override void Disconnect()
        {
            if (!_isConnected) return;
            _isConnected = false;

            Task.Run(async () =>
                {
                    await _host.StopAsync();
                    _host.Dispose();
                })
                .ContinueWith(t =>
                {
                    var ex = t.Exception?.GetBaseException() ?? t.Exception;
                    RaiseExceptionEvent(ex);
                }, TaskContinuationOptions.OnlyOnFaulted);
        }

        public class ChatHubClient : IClock, IHostedService
        {
            private readonly SignalRPhysicalClient _physicalClient;
            private readonly HubConnection _connection;

            public ChatHubClient(SignalRPhysicalClient physicalClient)
            {
                _physicalClient = physicalClient;

                _connection = new HubConnectionBuilder()
                    .WithUrl($"http://{physicalClient._address}:{physicalClient._port}/hubs/chat")
                    .WithAutomaticReconnect()
                    .Build();

                _connection.On<byte[]>(nameof(IClock.PublishMessage), PublishMessage);
            }

            public Task PublishMessage(byte[] message)
            {
                _physicalClient.RaiseBytesReceivedEvent(message);

                return Task.CompletedTask;
            }

            public Task RegisterMessage(byte[] message, Action<string> callback = null)
            {
                return _connection.InvokeAsync(nameof(RegisterMessage), message)
                    .ContinueWith(
                        task =>
                        {
                            if (!task.IsFaulted)
                            {
                                callback?.Invoke(null);
                                return;
                            }

                            var ex = task.Exception?.GetBaseException() ?? task.Exception;
                            _physicalClient.RaiseExceptionEvent(ex);
                        });
            }

            public async Task StartAsync(CancellationToken cancellationToken)
            {
                // Loop is here to wait until the server is running
                while (true)
                {
                    try
                    {
                        await _connection.StartAsync(cancellationToken);

                        break;
                    }
                    catch (Exception ex)
                    {
                        _physicalClient.RaiseExceptionEvent(ex);
                    }
                }

                _physicalClient.RaiseConnectedEvent();
            }

            public Task StopAsync(CancellationToken cancellationToken)
            {
                return _connection.DisposeAsync().AsTask();
            }
        }

        public interface IClock
        {
            Task PublishMessage(byte[] message);
            Task RegisterMessage(byte[] message, Action<string> callback = null);
        }
    }
}
