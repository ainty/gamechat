﻿using System.ComponentModel;
using System.Windows;

namespace GameClientWpfUI
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            DataContext = new LoginDialogViewModel(() => DialogResult = true);

            InitializeComponent();
        }

        private void Window_Closing(object? sender, CancelEventArgs e)
        {
        }
    }
}
