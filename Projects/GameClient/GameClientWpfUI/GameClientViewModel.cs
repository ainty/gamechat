﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using GameClientCore;

namespace GameClientWpfUI
{
    internal class GameClientViewModel : INotifyPropertyChanged
    {
        private readonly ILoginService _loginService;
        private readonly Action<string, string> _onLogEvent;
        private readonly Dictionary<string, PropertyChangedEventArgs> _eventArgs = new();

        private GameClient _gameClient;

        private bool _connected;
        private string _message = "";
        private string? _selectedGame;
        private string _title = typeof(GameClientViewModel).Namespace!;

        public bool Connected
        {
            get => _gameClient != default! && _connected;
            set => SetField(ref _connected, value);
        }

        public string Message
        {
            get => _message;
            set => SetField(ref _message, value);
        }

        public string? SelectedGame
        {
            get => _selectedGame;
            set => SetField(ref _selectedGame, value);
        }

        public string Title
        {
            get => _title;
            set => SetField(ref _title, value);
        }

        public RelayCommand SendMessageCommand => new(_ =>
        {
            _gameClient.Send(Message);
            Message = "";
        }, _ => Connected && Message.Length > 0);

        public RelayCommand ReloadGamesCommand => new(_ => _gameClient.LoadGames(), _ => Connected);
        public RelayCommand CreateGameCommand => new(_ => _gameClient.CreateAndJoinToGame(Guid.NewGuid().ToString()[..4]), _ => Connected && _gameClient.GameName == default);
        public RelayCommand JoinGameCommand => new(_ => _gameClient.JoinToGame(SelectedGame), _ => Connected && _gameClient.GameName == default && SelectedGame != default);

        public RelayCommand ReloadPlayersCommand => new(_ => _gameClient.LoadPlayers(), _ => Connected && _gameClient.GameName != default);
        public RelayCommand LeaveGameCommand => new(_ =>
        {
            _gameClient.LeaveGame();
            Players.Clear();
        }, _ => Connected && _gameClient.GameName != default);

        public RelayCommand ConnectCommand => new(_ =>
        {
            if (_gameClient == default!)
            {
                if (_loginService.TryLogin(out _gameClient))
                {
                    _gameClient.GameInEvent += msg => Application.Current.Dispatcher.Invoke(() => GameClient_GameIn(msg));
                    _gameClient.GameOutEvent += msg => Application.Current.Dispatcher.Invoke(() => GameClient_GameOut(msg));
                    _gameClient.LoginEvent += msg => Application.Current.Dispatcher.Invoke(() => GameClient_Login(msg));
                    _gameClient.LogoutEvent += msg => Application.Current.Dispatcher.Invoke(() => GameClient_Logout(msg));
                    _gameClient.PlayerListEvent += msg => Application.Current.Dispatcher.Invoke(() => GameClient_PlayerList(msg));
                    _gameClient.GameListEvent += msg => Application.Current.Dispatcher.Invoke(() => GameClient_GameList(msg));
                    _gameClient.MessageEvent += msg => Application.Current.Dispatcher.Invoke(() => GameClient_Message(msg));

                    _gameClient.LogEvent += (msg, caption) => Application.Current.Dispatcher.Invoke(() => GameClient_LogEvent(msg, caption));

                    Title = $"{_title} {_gameClient.UserName}";
                }
            }

            _gameClient.Connect();
            Connected = true;
        }, _ => !Connected);
        public RelayCommand DisconnectCommand => new(_ =>
        {
            _gameClient.Disconnect();
            Connected = false;
        }, _ => Connected);

        #region Game Client Event Handlers

        private void GameClient_Login(string text)
        {
            Players.Add(text);
        }

        private void GameClient_Logout(string text)
        {
            Players.Remove(text);
        }

        private void GameClient_PlayerList(string? content)
        {
            Players.Clear();
            if (content?.Length > 0)
            {
                var items = content.Split(new[] { '*' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var item in items)
                {
                    Players.Add(item);
                }
                Messages.Add($"<<<{_gameClient.UserName} has requested the room {_gameClient.GameName} players>>>\r\n");
            }
            else
            {
                Players.Add("no one found");
            }
        }

        private void GameClient_GameList(string? content)
        {
            Games.Clear();
            if (content?.Length > 0)
            {
                var items = content.Split(new[] { '*' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var item in items)
                {
                    Games.Add(item);
                }
            }
            else
            {
                Games.Add("no one found");
            }
        }

        private void GameClient_GameIn(string userName)
        {
            Players.Add(userName);
            Messages.Add($"<<<{userName} has joined the room {_gameClient.GameName}>>>\r\n");
        }

        private void GameClient_GameOut(string userName)
        {
            int i = Players.IndexOf(userName);
            if (i >= 0)
            {
                Players.RemoveAt(i);
            }
            Messages.Add($"<<<{userName} has left the room {_gameClient.GameName}>>>\r\n");
        }

        private void GameClient_Message(string content)
        {
            Messages.Add(content + Environment.NewLine);
        }

        private void GameClient_LogEvent(string msg, string caption)
        {
            _onLogEvent(msg, caption);
        }
        #endregion

        public GameClientViewModel(ILoginService loginDialogService, Action<string, string> onLogEvent)
        {
            _loginService = loginDialogService;
            this._onLogEvent = onLogEvent;
        }

        public ObservableCollection<string> Messages { get; set; } = new();
        public ObservableCollection<string> Games { get; set; } = new();
        public ObservableCollection<string> Players { get; set; } = new();


        public event PropertyChangedEventHandler? PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string? propertyName = null)
        {
            PropertyChanged?.Invoke(this,
                _eventArgs.TryGetValue(propertyName!, out var v)
                    ? v
                    : _eventArgs[propertyName!] = new PropertyChangedEventArgs(propertyName));
        }

        protected bool SetField<T>(ref T field, T value, [CallerMemberName] string? propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }
    }
}
