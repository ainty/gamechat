﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace GameClientWpfUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            DataContext = new GameClientViewModel(
                new LoginService(loginWindow => loginWindow.Owner = this),
                (msg, caption) => MessageBox.Show(this, msg, caption, MessageBoxButton.OK, MessageBoxImage.Error));

            InitializeComponent();
        }

        private void ScrollViewer_OnScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (e.OriginalSource is ScrollViewer scrollViewer && Math.Abs(e.ExtentHeightChange) > 0.0)
            {
                scrollViewer.ScrollToBottom();
            }
        }
    }
}
