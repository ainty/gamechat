﻿using GameClientCore;

namespace GameClientWpfUI;

public interface ILoginService
{
    bool TryLogin(out GameClient gameClient);
}