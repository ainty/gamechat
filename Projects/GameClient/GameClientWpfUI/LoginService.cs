﻿using System;
using ConnectionSchemes;
using Definitions;
using System.Net;
using GameClientCore;
using GameClientPhysical;

namespace GameClientWpfUI
{
    public class LoginService : ILoginService
    {
        private readonly Action<LoginWindow> _beforeShow;

        public LoginService(Action<LoginWindow> beforeShow)
        {
            _beforeShow = beforeShow;
        }

        public bool TryLogin(out GameClient gameClient)
        {
            gameClient = default!;

            var dialog = new LoginWindow();
            _beforeShow(dialog);

            var result = dialog.ShowDialog();
            if (result == true)
            {
                var vm = (dialog.DataContext as LoginDialogViewModel);
                var username = vm?.UserName!;
                var gateway = vm?.Server!;
                var way = vm?.Way!;
                var port = vm?.Port ?? 0;
                var protocol = vm?.Protocol!;
                gameClient = ComposeGameClientUserInterface(username, gateway, way, port, protocol);
            }

            return result == true;
        }

        private static GameClient ComposeGameClientUserInterface(string username, string gateway, string way, int port, string protocol)
        {
            var physicalClient = ComposePhysicalClient(gateway, way, port, protocol);

            var gameClient = new GameClient(physicalClient)
            {
                UserName = username
            };

            return gameClient;
        }

        private static IPhysicalClient ComposePhysicalClient(string gateway, string way, int port, string protocol)
        {
            var ipAddress = IPAddress.Parse(gateway);
            var endPointScheme = new EndPointScheme(ipAddress, port);
            var socketScheme = new SocketScheme(endPointScheme);

            var byProtocol = TypeHelper.Combine<Type>(
                TypeHelper.Has<ProtocolAttribute>,
                type => TypeHelper.Get<ProtocolAttribute>(type).Protocol == protocol);

            var filter = TypeHelper.Combine((Type t) => true);
            var endPointProvider = TypeHelper.Get<EndPointProvider>(filter, endPointScheme);

            filter = TypeHelper.Combine(byProtocol, TypeHelper.Is<SocketProvider>());
            var socketProvider = TypeHelper.Get<SocketProvider>(filter, socketScheme);

            filter = TypeHelper.Combine(byProtocol, TypeHelper.With<WayAttribute>(way));
            var physicalClient = TypeHelper.Get<IPhysicalClient>(filter, socketProvider, endPointProvider);

            return physicalClient;
        }
    }
}
