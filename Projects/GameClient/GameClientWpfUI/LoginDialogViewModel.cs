﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace GameClientWpfUI
{
    internal class LoginDialogViewModel : INotifyPropertyChanged
    {
        private readonly Action _onSuccess;

        private readonly Dictionary<string, PropertyChangedEventArgs> _eventArgs = new();

        private string userName = "";
        private string server = "";
        private int port;
        private string way = "";
        private string protocol = "";

        public LoginDialogViewModel(Action onSuccess)
        {
            _onSuccess = onSuccess;
        }

        public string UserName
        {
            get => userName;
            set => SetField(ref userName, value);
        }
        public string Server
        {
            get => server;
            set => SetField(ref server, value);
        }
        public int Port
        {
            get => port;
            set => SetField(ref port, value);
        }
        public string Way
        {
            get => way;
            set => SetField(ref way, value);
        }
        public string Protocol
        {
            get => protocol;
            set => SetField(ref protocol, value);
        }

        public RelayCommand LoginCommand => new(_ => _onSuccess(),
            _ => UserName.Length > 0 &&
                 Server.Length > 0 &&
                 Port > 0 &&
                 Way.Length > 0 &&
                 Protocol.Length > 0);

        public event PropertyChangedEventHandler? PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string? propertyName = null)
        {
            PropertyChanged?.Invoke(this,
                _eventArgs.TryGetValue(propertyName!, out var v)
                    ? v
                    : _eventArgs[propertyName!] = new PropertyChangedEventArgs(propertyName));
        }

        protected bool SetField<T>(ref T field, T value, [CallerMemberName] string? propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }
    }
}
