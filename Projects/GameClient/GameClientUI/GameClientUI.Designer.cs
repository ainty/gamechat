namespace GameClientUI
{
    partial class GameClientUi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GameClientUi));
            this.btnSend = new System.Windows.Forms.Button();
            this.txtChatBox = new System.Windows.Forms.TextBox();
            this.txtMessage = new System.Windows.Forms.TextBox();
            this.lstPlayers = new System.Windows.Forms.ListBox();
            this.lstGames = new System.Windows.Forms.ListBox();
            this.btnLoadGames = new System.Windows.Forms.Button();
            this.btnCreateOne = new System.Windows.Forms.Button();
            this.btnJoin = new System.Windows.Forms.Button();
            this.btnLoadPlayers = new System.Windows.Forms.Button();
            this.btnLeaveGame = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnSend
            // 
            resources.ApplyResources(this.btnSend, "btnSend");
            this.btnSend.Name = "btnSend";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // txtChatBox
            // 
            this.txtChatBox.BackColor = System.Drawing.SystemColors.Window;
            resources.ApplyResources(this.txtChatBox, "txtChatBox");
            this.txtChatBox.Name = "txtChatBox";
            this.txtChatBox.ReadOnly = true;
            // 
            // txtMessage
            // 
            resources.ApplyResources(this.txtMessage, "txtMessage");
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.TextChanged += new System.EventHandler(this.txtMessage_TextChanged);
            this.txtMessage.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMessage_KeyDown);
            // 
            // lstPlayers
            // 
            this.lstPlayers.BackColor = System.Drawing.SystemColors.Window;
            this.lstPlayers.FormattingEnabled = true;
            resources.ApplyResources(this.lstPlayers, "lstPlayers");
            this.lstPlayers.Name = "lstPlayers";
            // 
            // lstGames
            // 
            this.lstGames.BackColor = System.Drawing.SystemColors.Window;
            this.lstGames.ForeColor = System.Drawing.SystemColors.InfoText;
            this.lstGames.FormattingEnabled = true;
            resources.ApplyResources(this.lstGames, "lstGames");
            this.lstGames.Name = "lstGames";
            // 
            // btnLoadGames
            // 
            resources.ApplyResources(this.btnLoadGames, "btnLoadGames");
            this.btnLoadGames.Name = "btnLoadGames";
            this.btnLoadGames.UseVisualStyleBackColor = true;
            this.btnLoadGames.Click += new System.EventHandler(this.btnLoadGames_Click);
            // 
            // btnCreateOne
            // 
            resources.ApplyResources(this.btnCreateOne, "btnCreateOne");
            this.btnCreateOne.Name = "btnCreateOne";
            this.btnCreateOne.UseVisualStyleBackColor = true;
            this.btnCreateOne.Click += new System.EventHandler(this.btnCreateOne_Click);
            // 
            // btnJoin
            // 
            resources.ApplyResources(this.btnJoin, "btnJoin");
            this.btnJoin.Name = "btnJoin";
            this.btnJoin.UseVisualStyleBackColor = true;
            this.btnJoin.Click += new System.EventHandler(this.btnJoin_Click);
            // 
            // btnLoadPlayers
            // 
            resources.ApplyResources(this.btnLoadPlayers, "btnLoadPlayers");
            this.btnLoadPlayers.Name = "btnLoadPlayers";
            this.btnLoadPlayers.UseVisualStyleBackColor = true;
            this.btnLoadPlayers.Click += new System.EventHandler(this.btnLoadPlayers_Click);
            // 
            // btnLeaveGame
            // 
            resources.ApplyResources(this.btnLeaveGame, "btnLeaveGame");
            this.btnLeaveGame.Name = "btnLeaveGame";
            this.btnLeaveGame.UseVisualStyleBackColor = true;
            this.btnLeaveGame.Click += new System.EventHandler(this.btnLeaveGame_Click);
            // 
            // GameClientUI
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnLeaveGame);
            this.Controls.Add(this.btnLoadPlayers);
            this.Controls.Add(this.btnJoin);
            this.Controls.Add(this.btnCreateOne);
            this.Controls.Add(this.btnLoadGames);
            this.Controls.Add(this.lstGames);
            this.Controls.Add(this.lstPlayers);
            this.Controls.Add(this.txtMessage);
            this.Controls.Add(this.txtChatBox);
            this.Controls.Add(this.btnSend);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "GameClientUI";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GameClientUI_FormClosing);
            this.Load += new System.EventHandler(this.GameClientUI_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.TextBox txtChatBox;
        private System.Windows.Forms.TextBox txtMessage;
        private System.Windows.Forms.ListBox lstPlayers;
        private System.Windows.Forms.ListBox lstGames;
        private System.Windows.Forms.Button btnLoadGames;
        private System.Windows.Forms.Button btnCreateOne;
        private System.Windows.Forms.Button btnJoin;
        private System.Windows.Forms.Button btnLoadPlayers;
        private System.Windows.Forms.Button btnLeaveGame;
    }
}

