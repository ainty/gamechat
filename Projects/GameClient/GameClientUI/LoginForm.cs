using System;
using System.Collections.Generic;
using System.Net;
using System.Windows.Forms;
using Definitions;
using GameClientPhysical;

namespace GameClientUI
{
    public partial class LoginForm : Form
    {
        public string UserName;
        public string Server;
        public string Way;
        public int Port;
        public string Protocol;

        private readonly Dictionary<string, string> protocolVersions = new Dictionary<string, string>
        {
            {"V4", IPAddress.Loopback.ToString() },
            {"V6", IPAddress.IPv6Loopback.ToString() }
        };

        public LoginForm()
        {
            InitializeComponent();
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            UpdateSupportedWays();
            UpdateSupportedProtocols();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                UserName = txtName.Text;
                Server = txtServerIP.Text;
                Way = cboxWay.Text;
                Port = int.Parse(txtServerPort.Text);
                Protocol = cboxProtocol.Text;

                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "GameClient", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            btnOK.Enabled = txtName.Text.Length > 0 && txtServerIP.Text.Length > 0;
        }

        private void txtServerIP_TextChanged(object sender, EventArgs e)
        {
            btnOK.Enabled = txtName.Text.Length > 0 && txtServerIP.Text.Length > 0;
        }

        private void cboxProtocol_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtServerIP.Text = protocolVersions[cboxVersion.Text];
        }

        private IEnumerable<string> GetSupportedModes()
        {
            var result = new List<string>();

            foreach (var type in TypeHelper.GetImplementationsOf<IPhysicalClient>())
            {
                if (TypeHelper.Has<WayAttribute>(type))
                {
                    var mode = TypeHelper.Get<WayAttribute>(type).Mode;
                    if (!result.Contains(mode))
                    {
                        result.Add(mode);
                    }
                }
            }

            return result;
        }

        private IEnumerable<string> GetSupportedProtocols()
        {
            var result = new List<string>();

            foreach (var type in TypeHelper.GetImplementationsOf<IPhysicalClient>())
            {
                if (TypeHelper.Has<ProtocolAttribute>(type))
                {
                    var protocol = TypeHelper.Get<ProtocolAttribute>(type).Protocol;
                    if (!result.Contains(protocol))
                    {
                        result.Add(protocol);
                    }
                }
            }

            return result;
        }

        private void UpdateSupportedWays()
        {
            cboxWay.Items.Clear();

            foreach (var way in GetSupportedModes())
            {
                cboxWay.Items.Add(way);
                cboxWay.Text = way;
            }
        }

        private void UpdateSupportedProtocols()
        {
            cboxProtocol.Items.Clear();

            foreach (var protocol in GetSupportedProtocols())
            {
                cboxProtocol.Items.Add(protocol);
                cboxProtocol.Text = protocol;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}