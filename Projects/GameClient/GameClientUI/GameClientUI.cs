using System;
using System.Windows.Forms;
using GameClientCore;

namespace GameClientUI
{

    public partial class GameClientUi : Form
    {
        #region Fields

        private readonly GameClient gameClient;
        #endregion

        #region .ctor

        public GameClientUi(GameClient gameClient)
        {
            InitializeComponent();

            this.gameClient = gameClient;
        }
        #endregion

        #region Form Event Handlers

        private void GameClientUI_Load(object sender, EventArgs e)
        {
            Text = "GameClient: " + gameClient.UserName;

            gameClient.GameInEvent += msg => Invoke(() => gameClient_GameIn(msg));
            gameClient.GameOutEvent += msg => Invoke(() => gameClient_GameOut(msg));
            gameClient.LoginEvent += msg => Invoke(() => gameClient_Login(msg));
            gameClient.LogoutEvent += msg => Invoke(() => gameClient_Logout(msg));
            gameClient.PlayerListEvent += msg => Invoke(() => gameClient_PlayerList(msg));
            gameClient.GameListEvent += msg => Invoke(() => gameClient_GameList(msg));
            gameClient.MessageEvent += msg => Invoke(() => gameClient_Message(msg));

            gameClient.LogEvent += (msg, caption) => Invoke(() => gameClient_LogEvent(msg, caption));

            gameClient.Connect();

            OnGameOut();
        }

        private void GameClientUI_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to leave the chat room?", "GameClient: " + gameClient.UserName,
                MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.No)
            {
                e.Cancel = true;
                return;
            }

            try
            {
                gameClient.Disconnect();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "GameClient: " + gameClient.UserName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        } 
        #endregion

        #region UI Controls Handlers

        private void txtMessage_TextChanged(object sender, EventArgs e)
        {
            btnSend.Enabled = txtMessage.Text.Length != 0;
        }

        private void txtMessage_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSend_Click(sender, null);
            }
        } 
        #endregion

        #region Button Handlers

        private void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                gameClient.Send(txtMessage.Text);
                txtMessage.Text = null;
            }
            catch (Exception)
            {
                MessageBox.Show("Unable to send message to the server.", "GameClient: " + gameClient.UserName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCreateOne_Click(object sender, EventArgs e)
        {
            string gameName = "game" + DateTime.Now.Millisecond;
            gameClient.CreateAndJoinToGame(gameName);

            OnGameIn();
        }

        private void btnLoadGames_Click(object sender, EventArgs e)
        {
            gameClient.LoadGames();
        }

        private void btnLoadPlayers_Click(object sender, EventArgs e)
        {
            gameClient.LoadPlayers();
        }

        private void btnJoin_Click(object sender, EventArgs e)
        {
            var gameName = string.Empty;

            if (lstGames.SelectedItem != null)
            {
                gameName = Convert.ToString(lstGames.SelectedItem.ToString());
            }
            else if(lstGames.Items.Count > 0)
            {
                gameName = lstGames.Items[0].ToString();
            }

            if (!string.IsNullOrWhiteSpace(gameName))
            {
                gameClient.JoinToGame(gameName);
            }

            OnGameIn();
        }

        private void btnLeaveGame_Click(object sender, EventArgs e)
        {
            gameClient.LeaveGame();

            OnGameOut();
        } 
        #endregion

        #region Game Client Event Handlers

        private void gameClient_Login(string text)
        {
            lstPlayers.Items.Add(text);
        }

        private void gameClient_Logout(string text)
        {
            lstPlayers.Items.Remove(text);
        }

        private void gameClient_PlayerList(string content)
        {
            lstPlayers.Items.Clear();
            if (content != null)
            {
                var items = content.Split(new[] {'*'}, StringSplitOptions.RemoveEmptyEntries);
                foreach (var item in items)
                {
                    lstPlayers.Items.Add(item);
                }
                txtChatBox.Text += string.Format("<<<{0} has requested the room {1} players>>>\r\n",
                    gameClient.UserName, gameClient.GameName);
            }
            else
            {
                lstPlayers.Items.Add("no one found");
            }
        }

        private void gameClient_GameList(string content)
        {
            lstGames.Items.Clear();
            if (content != null)
            {
                var items = content.Split(new[] {'*'}, StringSplitOptions.RemoveEmptyEntries);
                foreach (var item in items)
                {
                    lstGames.Items.Add(item);
                }
            }
            else
            {
                lstGames.Items.Add("no one found");
            }
        }

        private void gameClient_GameIn(string userName)
        {
            lstPlayers.InvokeRequired.ToString();

            lstPlayers.Items.Add(userName);
            txtChatBox.Text += string.Format("<<<{0} has joined the room {1}>>>\r\n",
                userName, gameClient.GameName);
        }

        private void gameClient_GameOut(string userName)
        {
            int i = lstPlayers.Items.IndexOf(userName);
            if (i >= 0)
            {
                lstPlayers.Items.RemoveAt(i);
            }
            txtChatBox.Text += string.Format("<<<{0} has left the room {1}>>>\r\n",
                userName, gameClient.GameName);
        }

        private void gameClient_Message(string content)
        {
            txtChatBox.Text += content + Environment.NewLine;
        }

        private void gameClient_LogEvent(string msg, string caption)
        {
            MessageBox.Show(msg, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        #endregion

        #region Ui State

        private void OnGameOut()
        {
            btnLoadGames.Enabled = true;
            btnCreateOne.Enabled = true;
            btnJoin.Enabled = true;
            btnLoadPlayers.Enabled = false;
            btnLeaveGame.Enabled = false;
        }

        private void OnGameIn()
        {
            btnLoadGames.Enabled = true;
            btnCreateOne.Enabled = false;
            btnJoin.Enabled = false;
            btnLoadPlayers.Enabled = true;
            btnLeaveGame.Enabled = true;
        }

        #endregion
    }
}