using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConnectionSchemes;
using Definitions;
using GameClientCore;
using GameClientPhysical;

namespace GameClientUI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            TaskScheduler.UnobservedTaskException += TaskScheduler_UnobservedTaskException;
            Application.ThreadException += Application_ThreadException;

            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var loginForm = new LoginForm();
            loginForm.ShowDialog();
            if (loginForm.DialogResult == DialogResult.OK)
            {
                var login = loginForm;
                Application.Run(ComposeGameClientUserInterface(login.UserName, login.Server, login.Way, login.Port, login.Protocol));
            }
        }

        private static GameClientUi ComposeGameClientUserInterface(string username, string gateway, string way, int port, string protocol)
        {
            var physicalClient = ComposePhysicalClient(gateway, way, port, protocol);

            var gameClient = new GameClient(physicalClient)
            {
                UserName = username
            };

            return new GameClientUi(gameClient);
        }

        private static IPhysicalClient ComposePhysicalClient(string gateway, string way, int port, string protocol)
        {
            var ipAddress = IPAddress.Parse(gateway);
            var endPointScheme = new EndPointScheme(ipAddress, port);
            var socketScheme = new SocketScheme(endPointScheme);

            var byProtocol = TypeHelper.Combine<Type>(
                TypeHelper.Has<ProtocolAttribute>,
                type => TypeHelper.Get<ProtocolAttribute>(type).Protocol == protocol);

            var filter = TypeHelper.Combine((Type t) => true);
            var endPointProvider = TypeHelper.Get<EndPointProvider>(filter, endPointScheme);

            filter = TypeHelper.Combine(byProtocol, TypeHelper.Is<SocketProvider>());
            var socketProvider = TypeHelper.Get<SocketProvider>(filter, socketScheme);

            filter = TypeHelper.Combine(byProtocol, TypeHelper.With<WayAttribute>(way));
            var physicalClient = TypeHelper.Get<IPhysicalClient>(filter, socketProvider, endPointProvider);

            return physicalClient;
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            WriteLog(e.ExceptionObject as Exception, 1);
        }

        private static void TaskScheduler_UnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            WriteLog(e.Exception, 2);
        }

        private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            WriteLog(e.Exception, 3);
        }

        private static void WriteLog(Exception ex, int index)
        {
            using (var writer = new StreamWriter(@"D:\server" + index.ToString() + ".log"))
            {
                writer.Write(ex.StackTrace);
                writer.Flush();
            }
        }
    }
}
