﻿using System;
using System.Net.Sockets;
using ConnectionSchemes;
using Definitions;

namespace GameClientPhysical
{
    [Way("eap"), Protocol("tcp")]
    public sealed class TcpEapPhysicalClient : PhysicalClient
    {
        #region Fields

        private Socket clientSocket;
        private readonly byte[] buffer = new byte[BufferSize];

        #endregion

        #region ctor

        public TcpEapPhysicalClient(SocketProvider socketProvider, EndPointProvider gatewayEndPointProvider) : base(socketProvider, gatewayEndPointProvider)
        {
        }
        #endregion

        #region Public Methods

        public override void Connect()
        {
            try
            {
                clientSocket = SocketProvider.CreateSocket();

                var args = new SocketAsyncEventArgs();
                args.Completed += OnConnectAsync;
                args.RemoteEndPoint = GatewayEndPointProvider.CreateEndpoint();

                //Connect to the server
                clientSocket.ConnectAsync(args);
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }

        public override void Send(byte[] data, Action<string> callback = null)
        {
            try
            {
                var args = new SocketAsyncEventArgs();
                args.SetBuffer(data, 0, data.Length);
                args.UserToken = callback;
                args.Completed += OnSendAsync;
                clientSocket?.SendAsync(args);
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }

        public override void Disconnect()
        {
            try
            {
                clientSocket.Close();
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }
        #endregion

        #region Phisical Interaction Methods

        private void OnConnectAsync(object sender, SocketAsyncEventArgs e)
        {
            try
            {
                //We are connected so we login into the server
                RaiseConnectedEvent();

                var args = new SocketAsyncEventArgs();
                args.SetBuffer(buffer, 0, buffer.Length);
                args.Completed += OnReceiveAsync;

                clientSocket.ReceiveAsync(args);
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }

        private void OnSendAsync(object sender, SocketAsyncEventArgs e)
        {
            try
            {
                var callback = (Action<string>)e.UserToken;
                callback?.Invoke(null);
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }

        private void OnReceiveAsync(object sender, SocketAsyncEventArgs e)
        {
            try
            {
                RaiseBytesReceivedEvent(e.Buffer);

                var args = new SocketAsyncEventArgs();
                args.SetBuffer(buffer, 0, buffer.Length);
                args.Completed += OnReceiveAsync;

                clientSocket.ReceiveAsync(args);
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }
        #endregion
    }
}
