﻿using System;
using GameClientPhysical;
using Definitions;

namespace GameClientCore
{
    public class GameClient
    {
        #region Events
        public delegate void LogDelegate(string message, string title);
        public delegate void MsgDelegate(string message);

        public event MsgDelegate GameInEvent;
        public event MsgDelegate GameOutEvent;
        public event MsgDelegate LoginEvent;
        public event MsgDelegate LogoutEvent;
        public event MsgDelegate PlayerListEvent;
        public event MsgDelegate GameListEvent;
        public event MsgDelegate MessageEvent;

        public event LogDelegate LogEvent;
        #endregion

        #region Fields

        private readonly IPhysicalClient physicalClient;
        private readonly Serialization<Package> serialization;

        public string UserName;
        public string GameName;
        #endregion

        #region .ctor

        public GameClient(IPhysicalClient physicalClient)
        {
            if (physicalClient == null)
            {
                throw new ArgumentNullException(nameof(physicalClient));
            }

            this.physicalClient = physicalClient;
            serialization = new Serialization<Package>();

            // inform server when connected
            this.physicalClient.Connected += delegate
            {
                var buffer = serialization.GetBytes(new Package
                {
                    Action = GameActionType.LogIn,
                    UserName = UserName,
                    GameName = GameName,
                    Content = null
                });
                physicalClient.Send(buffer);
            };

            // perform received data
            this.physicalClient.BytesReceived += delegate (byte[] data)
            {
                var package = serialization.GetObject(data);
                ProcessPackage(package);
            };

            // perform error 
            this.physicalClient.ExceptionThrown += ex => RaiseLogEvent(ex.Message, "Physical Client " + UserName);
        }
        #endregion

        #region Public Methods

        public void Connect()
        {
            physicalClient.Connect();
        }

        public void JoinToGame(string gameName)
        {
            try
            {
                GameName = gameName;

                byte[] buffer = serialization.GetBytes(new Package
                {
                    Action = GameActionType.GameIn,
                    UserName = UserName,
                    GameName = GameName,
                    Content = null
                });

                //The user has logged into the system so we now request the server to send
                //the names of all users who are in the chat room

                physicalClient.Send(buffer, obj =>
                {
                    var callbackBuffer = serialization.GetBytes(new Package
                    {
                        Action = GameActionType.PlayersList,
                        UserName = UserName,
                        GameName = GameName,
                        Content = null
                    });

                    physicalClient.Send(callbackBuffer);
                });
            }
            catch (Exception ex)
            {
                RaiseLogEvent(ex.Message, "GameClient " + UserName);
            }
        }

        public void CreateAndJoinToGame(string gameName)
        {
            JoinToGame(gameName);
        }

        public void LoadGames()
        {
            try
            {
                byte[] buffer = serialization.GetBytes(new Package
                {
                    UserName = UserName,
                    GameName = null,
                    Content = null,
                    Action = GameActionType.GamesList
                });

                physicalClient.Send(buffer);
            }
            catch (Exception ex)
            {
                RaiseLogEvent(ex.Message, "GameClient " + UserName);
            }
        }

        public void LoadPlayers()
        {
            try
            {
                byte[] buffer = serialization.GetBytes(new Package
                {
                    UserName = UserName,
                    GameName = GameName,
                    Content = null,
                    Action = GameActionType.PlayersList
                });

                physicalClient.Send(buffer);
            }
            catch (Exception ex)
            {
                RaiseLogEvent(ex.Message, "GameClient " + UserName);
            }
        }

        public void Send(string text)
        {
            try
            {
                byte[] buffer = serialization.GetBytes(new Package
                {
                    UserName = UserName,
                    GameName = GameName,
                    Content = text,
                    Action = GameActionType.Message
                });

                physicalClient.Send(buffer);
            }
            catch (Exception ex)
            {
                RaiseLogEvent(ex.Message, "GameClient " + UserName);
            }
        }

        public void LeaveGame()
        {
            try
            {
                var buffer = serialization.GetBytes(new Package
                {
                    Action = GameActionType.GameOut,
                    UserName = UserName,
                    GameName = GameName,
                    Content = null
                });

                Action<string> callback = text =>
                {
                    GameName = null;

                    buffer = serialization.GetBytes(new Package
                    {
                        Action = GameActionType.GamesList,
                        UserName = UserName,
                        GameName = null,
                        Content = null
                    });

                    physicalClient.Send(buffer);
                };

                physicalClient.Send(buffer, callback);
            }
            catch (Exception ex)
            {
                RaiseLogEvent(ex.Message, "GameClient " + UserName);
            }
        }

        public void Disconnect()
        {
            try
            {
                byte[] buffer = serialization.GetBytes(new Package
                {
                    Action = GameActionType.LogOut,
                    GameName = GameName,
                    UserName = UserName,
                    Content = null
                });

                physicalClient.Send(buffer, obj =>
                {
                    physicalClient.Disconnect();
                });
            }
            catch (Exception ex)
            {
                RaiseLogEvent(ex.Message, "GameClient " + UserName);
            }
        }
        #endregion

        #region Event Methods

        private void ProcessPackage(Package incomingPackage)
        {
            switch (incomingPackage.Action)
            {
                case GameActionType.LogIn:
                    LoginEvent?.Invoke(incomingPackage.UserName);
                    break;
                case GameActionType.LogOut:
                    LogoutEvent?.Invoke(incomingPackage.UserName);
                    break;
                case GameActionType.Message:
                    break;
                case GameActionType.PlayersList:
                    PlayerListEvent?.Invoke(incomingPackage.Content);
                    break;
                case GameActionType.GamesList:
                    GameListEvent?.Invoke(incomingPackage.Content);
                    break;
                case GameActionType.GameIn:
                    GameInEvent?.Invoke(incomingPackage.UserName);
                    break;
                case GameActionType.GameOut:
                    GameOutEvent?.Invoke(incomingPackage.UserName);
                    break;
                case GameActionType.Null:
                default:
                    throw new ApplicationException(incomingPackage.Action.ToString());
            }

            if (incomingPackage.Content != null && incomingPackage.Action != GameActionType.PlayersList)
            {
                MessageEvent?.Invoke(incomingPackage.Content);
            }
        }

        private void RaiseLogEvent(string msg, string caption)
        {
            LogEvent?.Invoke(msg, caption);
        }
        #endregion
    }
}
