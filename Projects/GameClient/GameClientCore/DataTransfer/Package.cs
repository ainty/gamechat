using System;

namespace GameClientCore
{
    class Package
    {
        public Package()
        {
            Action = GameActionType.Null;
            Content = null;
            UserName = null;
            GameName = null;
        }

        public GameActionType Action;
        public string UserName;
        public string GameName;
        public string Content;
    }
}
