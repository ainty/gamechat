﻿using System;
using ConnectionSchemes;

namespace GameClientPhysical
{
    public abstract class PhysicalClient : IPhysicalClient
    {
        protected const int BufferSize = 1024;
        protected readonly SocketProvider SocketProvider;
        protected readonly EndPointProvider GatewayEndPointProvider;

        protected PhysicalClient(SocketProvider socketProvider, EndPointProvider gatewayEndPointProvider)
        {
            SocketProvider = socketProvider 
                ?? throw new ArgumentNullException(nameof(socketProvider));

            GatewayEndPointProvider = gatewayEndPointProvider 
                ?? throw new ArgumentNullException(nameof(gatewayEndPointProvider));
        }

        public event BytesReceivedDelegate BytesReceived = data => {};

        public event ConnectedDelegate Connected = () => {};

        public event ExceptionThrownDelegate ExceptionThrown = ex => {};

        public abstract void Connect();

        public abstract void Send(byte[] data, Action<string> callback = null);

        public abstract void Disconnect();

        protected void RaiseConnectedEvent()
        {
            Connected.Invoke();
        }

        protected void RaiseBytesReceivedEvent(byte[] buffer)
        {
            BytesReceived.Invoke(buffer);
        }

        protected void RaiseExceptionEvent(Exception ex)
        {
            ExceptionThrown.Invoke(ex);
        }
    }
}
