﻿using System;

namespace GameClientPhysical
{
    public delegate void ConnectedDelegate();
    public delegate void BytesReceivedDelegate(byte[] data);
    public delegate void ExceptionThrownDelegate(Exception ex);

    public interface IPhysicalClient
    {
        event BytesReceivedDelegate BytesReceived;

        event ConnectedDelegate Connected;

        event ExceptionThrownDelegate ExceptionThrown;

        void Connect();

        void Send(byte[] data, Action<string> callback = null);

        void Disconnect();
    }
}
