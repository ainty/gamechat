﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using ConnectionSchemes;
using Definitions;

namespace GameClientPhysical
{
    [Way("tap"), Protocol("tcp")]
    public sealed class TcpTapPhysicalClient : PhysicalClient
    {
        #region Fields

        private TcpClient tcpClient;
        private readonly byte[] buffer = new byte[BufferSize];

        #endregion

        #region ctor

        public TcpTapPhysicalClient(SocketProvider socketProvider, EndPointProvider gatewayEndPointProvider) : base(socketProvider, gatewayEndPointProvider)
        {
        }
        #endregion

        #region Public Methods

        public override void Connect()
        {
            try
            {
                var clientSocket = SocketProvider.CreateSocket();
                tcpClient = new TcpClient(clientSocket.AddressFamily);
                
                var remoteEndPoint = GatewayEndPointProvider.CreateEndpoint() as IPEndPoint;
                
                Debug.Assert(remoteEndPoint != null, "remoteEndPoint != null");
                
                var address = remoteEndPoint.Address;
                var port = remoteEndPoint.Port;

                //Connect to the server
                tcpClient.ConnectAsync(address, port)
                    .ContinueWith(t => OnConnect(tcpClient.GetStream()));
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }

        public override void Send(byte[] data, Action<string> callback = null)
        {
            try
            {
                tcpClient?.GetStream()
                    .WriteAsync(data, 0, data.Length)
                    .ContinueWith(t => OnSend(callback));
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }

        public override void Disconnect()
        {
            try
            {
                tcpClient.Close();
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }
        #endregion

        #region Phisical Interaction Methods

        private void OnConnect(NetworkStream stream)
        {
            try
            {
                //We are connected so we login into the server
                RaiseConnectedEvent();

                stream
                    .ReadAsync(buffer, 0, buffer.Length)
                    .ContinueWith(t => OnReceive(stream));
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }

        private void OnReceive(NetworkStream stream)
        {
            try
            {
                RaiseBytesReceivedEvent(buffer);

                stream
                    .ReadAsync(buffer, 0, buffer.Length)
                    .ContinueWith(t => OnReceive(stream));
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }

        private void OnSend(Action<string> callback)
        {
            try
            {
                callback?.Invoke(null);
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }
        #endregion
    }
}
