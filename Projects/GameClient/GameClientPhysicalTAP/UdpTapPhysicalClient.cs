﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using ConnectionSchemes;
using Definitions;

namespace GameClientPhysical
{
    [Way("tap"), Protocol("udp")]
    public sealed class UdpTapPhysicalClient : PhysicalClient
    {
        #region Fields

        private UdpClient udpClient;
        #endregion

        #region ctor

        public UdpTapPhysicalClient(SocketProvider socketProvider, EndPointProvider gatewayEndPointProvider) : base(socketProvider, gatewayEndPointProvider)
        {
        }
        #endregion

        #region Public Methods

        public override void Connect()
        {
            try
            {
                var clientSocket = SocketProvider.CreateSocket();
                udpClient = new UdpClient(clientSocket.AddressFamily);

                var remoteEndPoint = GatewayEndPointProvider.CreateEndpoint() as IPEndPoint;

                Debug.Assert(remoteEndPoint != null, "remoteEndPoint != null");

                var address = remoteEndPoint.Address;
                var port = remoteEndPoint.Port;

                //Connect to the server
                udpClient.Connect(address, port);
                OnConnect(udpClient);
                udpClient.ReceiveAsync().ContinueWith(OnReceive);
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }

        public override void Send(byte[] data, Action<string> callback = null)
        {
            try
            {
                udpClient?.SendAsync(data, data.Length)
                    .ContinueWith(t => OnSend(callback));
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }

        public override void Disconnect()
        {
            try
            {
                udpClient.Close();
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }
        #endregion

        #region Phisical Interaction Methods

        private void OnConnect(UdpClient client)
        {
            try
            {
                //We are connected so we login into the server
                RaiseConnectedEvent();

                client.ReceiveAsync().ContinueWith(OnReceive);
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }

        private void OnReceive(Task<UdpReceiveResult> task)
        {
            try
            {
                RaiseBytesReceivedEvent(task.Result.Buffer);

                udpClient.ReceiveAsync().ContinueWith(OnReceive);
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }

        private void OnSend(Action<string> callback)
        {
            try
            {
                callback?.Invoke(null);
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }
        #endregion
    }
}
