﻿using System;
using System.Net;
using System.Net.Sockets;
using ConnectionSchemes;
using Definitions;

namespace GameClientPhysical
{
    [Way("apm"), Protocol("udp")]
    public sealed class UdpApmPhysicalClient : PhysicalClient
    {
        #region Fields

        private Socket clientSocket;
        private EndPoint gatewayEndPoint;
        #endregion

        #region ctor

        public UdpApmPhysicalClient(SocketProvider socketProvider, EndPointProvider gatewayEndPointProvider) : base(socketProvider, gatewayEndPointProvider)
        {
        }
        #endregion

        #region Public Methods

        public override void Connect()
        {
            try
            {
                clientSocket = SocketProvider.CreateSocket();

                gatewayEndPoint = GatewayEndPointProvider.CreateEndpoint();

                //Connect to the server
                // Send data to server
                var data = new byte[BufferSize];

                clientSocket.BeginSendTo(data, 0, data.Length, SocketFlags.None, gatewayEndPoint, OnConnect, null);
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }

        public override void Send(byte[] data, Action<string> callback = null)
        {
            try
            {
                clientSocket?.BeginSendTo(data, 0, data.Length, SocketFlags.None, gatewayEndPoint, OnSend, callback);
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }

        public override void Disconnect()
        {
            try
            {
                clientSocket.Close();
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }
        #endregion

        #region Phisical Interaction Methods

        private void OnConnect(IAsyncResult ar)
        {
            try
            {
                clientSocket.EndSendTo(ar);

                //We are connected so we login into the server
                RaiseConnectedEvent();

                var readBuffer = new byte[BufferSize];
                //Start listening to the data asynchronously
                // Begin listening for broadcasts
                clientSocket.BeginReceiveFrom(readBuffer, 0, readBuffer.Length, SocketFlags.None, ref gatewayEndPoint, OnReceive, readBuffer);
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }

        private void OnSend(IAsyncResult ar)
        {
            try
            {
                clientSocket.EndSendTo(ar);

                var callback = (Action<string>)ar.AsyncState;
                callback?.Invoke(null);
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }

        private void OnReceive(IAsyncResult ar)
        {
            try
            {
                clientSocket.EndReceiveFrom(ar, ref gatewayEndPoint);

                var readBuffer = (byte[])ar.AsyncState;

                RaiseBytesReceivedEvent(readBuffer);
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
            finally
            {
                var readBuffer = (byte[])ar.AsyncState;

                // Continue listening for broadcasts
                try
                {
                    clientSocket.BeginReceiveFrom(readBuffer, 0, readBuffer.Length, SocketFlags.None, ref gatewayEndPoint, OnReceive, readBuffer);
                }
                catch (ObjectDisposedException ex)
                {
                    RaiseExceptionEvent(ex);
                }
            }
        }
        #endregion
    }
}
