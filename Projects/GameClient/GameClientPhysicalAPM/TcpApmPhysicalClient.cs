﻿using System;
using System.Net.Sockets;
using ConnectionSchemes;
using Definitions;

namespace GameClientPhysical
{
    [Way("apm"), Protocol("tcp")]
    public sealed class TcpApmPhysicalClient : PhysicalClient
    {
        #region Fields

        private Socket clientSocket;

        private byte[] readBuffer = new byte[BufferSize];
        #endregion

        #region ctor

        public TcpApmPhysicalClient(SocketProvider socketProvider, EndPointProvider gatewayEndPointProvider) : base(socketProvider, gatewayEndPointProvider)
        {
        }
        #endregion

        #region Public Methods

        public override void Connect()
        {
            try
            {
                clientSocket = SocketProvider.CreateSocket();

                var gatewayEndPoint = GatewayEndPointProvider.CreateEndpoint();

                // Connect to the server
                clientSocket.BeginConnect(gatewayEndPoint, OnConnect, null);
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }

        public override void Send(byte[] data, Action<string> callback = null)
        {
            try
            {
                clientSocket?.BeginSend(data, 0, data.Length, SocketFlags.None, OnSend, callback);
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }

        public override void Disconnect()
        {
            try
            {
                clientSocket.Close();
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }
        #endregion

        #region Phisical Interaction Methods

        private void OnConnect(IAsyncResult ar)
        {
            try
            {
                clientSocket.EndConnect(ar);

                RaiseConnectedEvent();

                readBuffer = new byte[BufferSize];
                clientSocket.BeginReceive(readBuffer, 0, readBuffer.Length, SocketFlags.None, OnReceive, null);
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }

        private void OnSend(IAsyncResult ar)
        {
            try
            {
                clientSocket.EndSend(ar);

                Action<string> callback = (Action<string>)ar.AsyncState;
                callback?.Invoke(null);
            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }

        private void OnReceive(IAsyncResult ar)
        {
            try
            {
                clientSocket.EndReceive(ar);

                RaiseBytesReceivedEvent(readBuffer);

                readBuffer = new byte[BufferSize];
                clientSocket.BeginReceive(readBuffer, 0, readBuffer.Length, SocketFlags.None, OnReceive, null);

            }
            catch (Exception ex)
            {
                RaiseExceptionEvent(ex);
            }
        }
        #endregion
    }
}
