﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Threading;
using ConnectionSchemes;
using Definitions;

namespace GameServerPhysical
{
    [Way("eap"), Protocol("tcp")]
    public sealed class TcpEapPhysicalServer : PhysicalServer
    {
        #region Fields

        private Socket gameServerGatewaySocket;

        private delegate void SocketActionHandler(SocketAsyncEventArgs e);

        private readonly SocketActionHandler[] _handlers;

        private readonly BufferManager _bufferManager;
        private readonly int _numConnections;

        private readonly ConcurrentStack<SocketAsyncEventArgs> _readWritePool = new();
        private readonly SemaphoreSlim _maxNumberAcceptedClients;
        #endregion

        #region ctor

        public TcpEapPhysicalServer(SocketProvider socketProvider, EndPointProvider endPointProvider) : base(socketProvider, endPointProvider)
        {
            _handlers = new SocketActionHandler[1 + Math.Max((int)SocketAsyncOperation.Send, (int)SocketAsyncOperation.Receive)];
            _handlers[(int)SocketAsyncOperation.Send] = OnSendAsync;
            _handlers[(int)SocketAsyncOperation.Receive] = OnReceiveAsync;

            _numConnections = 100;
            const int readWriteCoefficient = 2;

            _bufferManager = new BufferManager(_numConnections * readWriteCoefficient, BufferSize);

            _maxNumberAcceptedClients = new SemaphoreSlim(_numConnections);
        }
        #endregion

        #region Public Methods

        public override void Send(object target, byte[] message)
        {
            try
            {
                if (target is not Socket userSocket) throw UserSocketNullException(nameof(target));

                _readWritePool.TryPop(out var args);
                args.UserToken = userSocket;
                CopyMessageToArgsBuffer(args, message);

                var willRaiseEvent = userSocket.SendAsync(args);
                if (!willRaiseEvent)
                {
                    OnSendAsync(args);
                }
            }
            catch (Exception ex)
            {
                RaiseExceptionThrown(ex);
            }
        }

        public override void Start()
        {
            //We are using TCP sockets
            gameServerGatewaySocket = SocketProvider.CreateSocket();

            //Assign the any IP of the machine and listen on port number 1000
            var gatewayEndPoint = EndPointProvider.CreateEndpoint();

            for (var i = 0; i < _numConnections; i++)
            {
                var reusableReadWriteEventArgs = new SocketAsyncEventArgs();
                reusableReadWriteEventArgs.Completed += (_, e) => _handlers[(int)e.LastOperation].Invoke(e);

                _bufferManager.AssignByteBuffer(reusableReadWriteEventArgs);

                _readWritePool.Push(reusableReadWriteEventArgs);
            }

            gameServerGatewaySocket.Bind(gatewayEndPoint);
            gameServerGatewaySocket.Listen(100);

            var acceptEventArg = new SocketAsyncEventArgs();
            acceptEventArg.Completed += (_, e) =>
            {
                OnAcceptAsync(e);
                WaitForTcpClient(e);
            };
            WaitForTcpClient(acceptEventArg);
        }

        public override void Stop()
        {
            gameServerGatewaySocket?.Close();
            gameServerGatewaySocket = null;
        }
        #endregion

        #region Physical Interaction Methods

        private void WaitForTcpClient(SocketAsyncEventArgs acceptEventArg)
        {
            // loop while the method completes synchronously
            var willRaiseEvent = false;
            while (!willRaiseEvent)
            {
                _maxNumberAcceptedClients.Wait();

                // socket must be cleared since the context object is being reused
                acceptEventArg.AcceptSocket = null;
                willRaiseEvent = gameServerGatewaySocket.AcceptAsync(acceptEventArg);
                if (!willRaiseEvent)
                {
                    OnAcceptAsync(acceptEventArg);
                }
            }
        }

        private void OnAcceptAsync(SocketAsyncEventArgs e)
        {
            try
            {
                _readWritePool.TryPop(out var readEventArgs);
                readEventArgs.UserToken = e.AcceptSocket;

                var willRaiseEvent = e.AcceptSocket.ReceiveAsync(readEventArgs);
                if (!willRaiseEvent)
                {
                    OnReceiveAsync(readEventArgs);
                }
            }
            catch (Exception ex)
            {
                RaiseExceptionThrown(ex);
            }
        }

        private void OnReceiveAsync(SocketAsyncEventArgs e)
        {
            var willRaiseEvent = false;
            while (!willRaiseEvent)
            {
                try
                {

                    if (!(e.BytesTransferred > 0 && e.SocketError == SocketError.Success))
                    {
                        CloseClientSocket(e);
                        break;
                    }

                    var buffer = new byte[BufferSize];
                    for (var i = 0; i < e.BytesTransferred; i++)
                    {
                        buffer[i] = e.Buffer[e.Offset + i];
                    }

                    if (e.UserToken is Socket { Connected: true } socket)
                    {
                        RaiseBytesReceived(socket, buffer);

                        willRaiseEvent = socket.ReceiveAsync(e);
                    }
                }
                catch (ObjectDisposedException)
                {
                }
                catch (Exception ex)
                {
                    RaiseExceptionThrown(ex);
                }
            }
        }

        private void OnSendAsync(SocketAsyncEventArgs e)
        {
            try
            {
                if (e.SocketError == SocketError.Success && e.UserToken is Socket { Connected: true } socket)
                {
                    var willRaiseEvent = socket.ReceiveAsync(e);
                    if (!willRaiseEvent)
                    {
                        OnReceiveAsync(e);
                    }
                }
                else
                {
                    CloseClientSocket(e);
                }
            }
            catch (Exception ex)
            {
                RaiseExceptionThrown(ex);
            }
        }

        private void CloseClientSocket(SocketAsyncEventArgs e)
        {
            try
            {
                var socket = (Socket)e.UserToken;

                try
                {
                    socket.Shutdown(SocketShutdown.Send);
                }
                catch (ObjectDisposedException)
                {
                }

                socket.Close();

                _readWritePool.Push(e);

                _maxNumberAcceptedClients.Release();

                _bufferManager.FreeBuffer(e);
            }
            catch (SemaphoreFullException)
            {
            }
            catch (ObjectDisposedException)
            {
            }
            catch (Exception ex)
            {
                RaiseExceptionThrown(ex);
            }
        }

        protected override bool IsOnline()
        {
            return gameServerGatewaySocket != null;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static void CopyMessageToArgsBuffer(SocketAsyncEventArgs args, byte[] message)
        {
            var i = 0;
            for (; i < message.Length; i++)
            {
                args.Buffer[args.Offset + i] = message[i];
            }

            for (; i < BufferSize; i++)
            {
                args.Buffer[args.Offset + i] = 0;
            }
        }

        private class BufferManager
        {
            private readonly int _totalControlledBytesCount;
            private readonly byte[] _buffer;
            private readonly int _bufferSize;

            private readonly Stack<int> _freeIndexPool = new();

            private int _currentIndex;

            public BufferManager(int buffersCount, int bufferSize)
            {
                _totalControlledBytesCount = buffersCount * bufferSize;
                _buffer = new byte[_totalControlledBytesCount];
                _bufferSize = bufferSize;
            }

            public bool AssignByteBuffer(SocketAsyncEventArgs args)
            {
                if (_freeIndexPool.Count > 0)
                {
                    args.SetBuffer(_buffer, _freeIndexPool.Pop(), _bufferSize);
                }
                else
                {
                    if (_totalControlledBytesCount - _bufferSize < _currentIndex)
                    {
                        return false;
                    }

                    args.SetBuffer(_buffer, _currentIndex, _bufferSize);
                    _currentIndex += _bufferSize;
                }

                return true;
            }

            public void FreeBuffer(SocketAsyncEventArgs args)
            {
                _freeIndexPool.Push(args.Offset);
                args.SetBuffer(null, 0, 0);
            }
        }

        #endregion
    }
}
