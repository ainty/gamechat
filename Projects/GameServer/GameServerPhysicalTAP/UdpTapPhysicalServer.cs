﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using ConnectionSchemes;
using Definitions;

namespace GameServerPhysical
{
    [Way("tap"), Protocol("udp")]
    public sealed class UdpTapPhysicalServer : PhysicalServer
    {
        #region Fields

        private UdpClient gameServerGatewayListener;
        #endregion

        #region ctor

        public UdpTapPhysicalServer(SocketProvider socketProvider, EndPointProvider endPointProvider) : base(socketProvider, endPointProvider)
        {
        }
        #endregion

        #region Public Methods

        public override void Send(object target, byte[] message)
        {
            var endPoint = target as IPEndPoint;
            if (endPoint == null) throw UserSocketNullException(nameof(target));

            gameServerGatewayListener.SendAsync(message, message.Length, endPoint);
        }

        public override void Start()
        {
            var gatewayEndPoint = EndPointProvider.CreateEndpoint();

            gameServerGatewayListener = new UdpClient((IPEndPoint)gatewayEndPoint);

            WaitForUdpClient(gameServerGatewayListener);
        }

        private void WaitForUdpClient(UdpClient udpListener)
        {
            udpListener.ReceiveAsync().ContinueWith(t =>
            {
                OnReceive(t.Result);
                WaitForUdpClient(udpListener);
            }, TaskContinuationOptions.OnlyOnRanToCompletion);
        }

        public override void Stop()
        {
            gameServerGatewayListener?.Close();
            gameServerGatewayListener = null;
        }

        #endregion

        private void OnReceive(UdpReceiveResult result)
        {
            if (IsOnline())
            {
                try
                {
                    var playerEndpoint = result.RemoteEndPoint;

                    RaiseBytesReceived(playerEndpoint, result.Buffer);
                }
                catch (Exception ex)
                {
                    RaiseExceptionThrown(ex);
                }
                finally
                {
                    // Continue listening to the message send by the user
                    gameServerGatewayListener
                        .ReceiveAsync()
                        .ContinueWith(t => OnReceive(t.Result), TaskContinuationOptions.OnlyOnRanToCompletion);
                }
            }
        }

        protected override bool IsOnline()
        {
            return gameServerGatewayListener != null;
        }
    }
}
