﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using ConnectionSchemes;
using Definitions;

namespace GameServerPhysical
{
    [Way("tap"), Protocol("tcp")]
    public sealed class TcpTapPhysicalServer : PhysicalServer
    {
        #region Fields

        private TcpListener gameServerGatewayListener;
        #endregion

        #region ctor

        public TcpTapPhysicalServer(SocketProvider socketProvider, EndPointProvider endPointProvider) 
            : base(socketProvider, endPointProvider)
        {
        }
        #endregion

        #region Public Methods

        public override void Send(object target, byte[] message)
        {
            if (target is not Socket userSocket) throw UserSocketNullException(nameof(target));

            var args = new SocketAsyncEventArgs();
            args.SetBuffer(message, 0, message.Length);
            userSocket.SendAsync(args);
        }

        public override void Start()
        {
            var gatewayEndPoint = EndPointProvider.CreateEndpoint();

            gameServerGatewayListener = new TcpListener((IPEndPoint)gatewayEndPoint);

            gameServerGatewayListener.Start(Environment.ProcessorCount);

            WaitForTcpClient(gameServerGatewayListener);
        }

        private void WaitForTcpClient(TcpListener tcpListener)
        {
            tcpListener.AcceptTcpClientAsync().ContinueWith(t =>
            {
                OnAccept(t.Result);
                WaitForTcpClient(tcpListener);
            }, TaskContinuationOptions.OnlyOnRanToCompletion);
        }

        public override void Stop()
        {
            gameServerGatewayListener?.Stop();
            gameServerGatewayListener = null;
        }
        #endregion

        private void OnAccept(TcpClient tcpClient)
        {
            if (IsOnline())
            {
                try
                {
                    var buffer = new byte[BufferSize];

                    //Once the client connects then start receiving the commands from her
                    tcpClient
                        .GetStream()
                        .ReadAsync(buffer, 0, buffer.Length)
                        .ContinueWith(t => OnReceive(tcpClient, buffer));
                }
                catch (Exception ex)
                {
                    RaiseExceptionThrown(ex);
                }
            }
        }

        private void OnReceive(TcpClient tcpClient, byte[] buffer)
        {
            if (IsOnline())
            {
                try
                {
                    var playerSocket = tcpClient.Client;

                    RaiseBytesReceived(playerSocket, buffer);

                    if (IsConnected(playerSocket))
                    {
                        // Continue listening to the message send by the user
                        tcpClient
                            .GetStream()
                            .ReadAsync(buffer, 0, buffer.Length)
                            .ContinueWith(t => OnReceive(tcpClient, buffer));
                    }
                    else
                    {
                        playerSocket.Close();
                    }
                }
                catch (Exception ex)
                {
                    tcpClient
                        .GetStream()
                        .ReadAsync(buffer, 0, buffer.Length)
                        .ContinueWith(t => OnReceive(tcpClient, buffer));

                    RaiseExceptionThrown(ex);
                }
            }
        }

        private static bool IsConnected(Socket socket)
        {
            try
            {
                return !socket.Poll(1, SelectMode.SelectRead) || socket.Available != 0;
            }
            catch (SocketException)
            {
                return false;
            }
        }

        protected override bool IsOnline()
        {
            return gameServerGatewayListener != null;
        }
    }
}
