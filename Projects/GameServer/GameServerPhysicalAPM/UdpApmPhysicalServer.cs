﻿using System;
using System.Net;
using System.Net.Sockets;
using ConnectionSchemes;
using Definitions;

namespace GameServerPhysical
{
    [Way("apm"), Protocol("udp")]
    public sealed class UdpApmPhysicalServer : PhysicalServer
    {
        #region Fields
        private Socket gameServerGatewayListener;
        private readonly byte[] buffer = new byte[BufferSize];
        #endregion

        #region ctor

        public UdpApmPhysicalServer(SocketProvider socketProvider, EndPointProvider endPointProvider) : base(socketProvider, endPointProvider)
        {
        }
        #endregion

        #region Public Methods

        public override void Send(object target, byte[] message)
        {
            var endPoint = target as IPEndPoint;
            if (endPoint == null) throw UserSocketNullException(nameof(target));

            gameServerGatewayListener.BeginSendTo(message, 0, message.Length, SocketFlags.None, endPoint, OnSend, endPoint);
        }

        public override void Start()
        {
            gameServerGatewayListener = SocketProvider.CreateSocket();

            var gatewayEndPoint = EndPointProvider.CreateEndpoint();

            // Bind to given address
            gameServerGatewayListener.Bind(gatewayEndPoint);

            // Initialize the IPEndPoint for the clients
            // var remoteEndpoint = EndPointProvider.CreateEndpointForIncoming();

            // Start listening for incoming data
            WaitForUdpClient(gameServerGatewayListener);
        }

        private void WaitForUdpClient(Socket udpListener)
        {
            if (IsOnline())
            {
                var clientEndpoint = EndPointProvider.CreateEndpointForIncoming();

                AsyncCallback callback = delegate (IAsyncResult asyncResult)
                {
                    OnReceive(asyncResult);
                    WaitForUdpClient(udpListener);
                };


                udpListener?.BeginReceiveFrom(buffer, 0, buffer.Length, SocketFlags.None, ref clientEndpoint, callback, clientEndpoint);
            }
        }

        public override void Stop()
        {
            gameServerGatewayListener?.Close();
            gameServerGatewayListener = null;
        }

        #endregion

        #region Phisical Interaction Methods

        private void OnReceive(IAsyncResult ar)
        {
            if (IsOnline())
            {
                try
                {
                    var playerEndPoint = (EndPoint)ar.AsyncState;
                    gameServerGatewayListener.EndReceiveFrom(ar, ref playerEndPoint);

                    RaiseBytesReceived(playerEndPoint, buffer);

                    // Continue listening to a messages
                    gameServerGatewayListener?.BeginReceiveFrom(buffer, 0, buffer.Length, SocketFlags.None,
                        ref playerEndPoint, OnReceive, playerEndPoint);
                }
                catch (Exception ex)
                {
                    var playerEndPoint = (EndPoint)ar.AsyncState;

                    gameServerGatewayListener?.BeginReceiveFrom(buffer, 0, buffer.Length, SocketFlags.None,
                        ref playerEndPoint, OnReceive, playerEndPoint);

                    RaiseExceptionThrown(ex);
                }
            }
        }

        private void OnSend(IAsyncResult ar)
        {
            try
            {
                gameServerGatewayListener.EndSendTo(ar);
            }
            catch (Exception ex)
            {
                RaiseExceptionThrown(ex);
            }
        }

        protected override bool IsOnline()
        {
            return gameServerGatewayListener != null;
        }

        #endregion
    }
}
