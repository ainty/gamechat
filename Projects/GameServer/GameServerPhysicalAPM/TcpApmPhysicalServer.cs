﻿using System;
using System.Net.Sockets;
using ConnectionSchemes;
using Definitions;

namespace GameServerPhysical
{
    [Way("apm"), Protocol("tcp")]
    public sealed class TcpApmPhysicalServer : PhysicalServer
    {
        #region Fields

        private Socket gameServerGatewaySocket;
        #endregion

        #region ctor

        public TcpApmPhysicalServer(SocketProvider socketProvider, EndPointProvider endPointProvider) : base(socketProvider, endPointProvider)
        {
        }
        #endregion

        #region Public Methods

        public override void Send(object target, byte[] message)
        {
            var userSocket = target as Socket;
            if (userSocket == null) throw UserSocketNullException(nameof(target));

            userSocket.BeginSend(message, 0, message.Length, SocketFlags.None, OnSend, userSocket);
        }

        public override void Start()
        {
            //We are using TCP sockets
            gameServerGatewaySocket = SocketProvider.CreateSocket();

            //Assign the any IP of the machine and listen on port number 1000
            var gatewayEndPoint = EndPointProvider.CreateEndpoint();

            //Bind and listen on the given address
            gameServerGatewaySocket.Bind(gatewayEndPoint);
            gameServerGatewaySocket.Listen(Environment.ProcessorCount);

            //Accept the incoming clients
            gameServerGatewaySocket.BeginAccept(OnAccept, null);
        }

        public override void Stop()
        {
            gameServerGatewaySocket?.Close();
            gameServerGatewaySocket = null;
        }
        #endregion

        #region Physical Interaction Methods

        private void OnAccept(IAsyncResult ar)
        {
            if (IsOnline())
            {
                try
                {

                    var playerSocket = gameServerGatewaySocket.EndAccept(ar);

                    //Start listening for more clients
                    gameServerGatewaySocket.BeginAccept(OnAccept, null);

                    byte[] buffer = new byte[BufferSize];

                    //Once the client connects then start receiving the commands from her
                    playerSocket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, OnReceive, Tuple.Create(playerSocket, buffer));

                }
                catch (Exception ex)
                {
                    RaiseExceptionThrown(ex);
                }
            }
        }

        private void OnReceive(IAsyncResult ar)
        {
            if (IsOnline())
            {
                try
                {
                    var state = (Tuple<Socket, byte[]>)ar.AsyncState;
                    var playerSocket = state.Item1;
                    playerSocket.EndReceive(ar);

                    var buffer = state.Item2;
                    RaiseBytesReceived(playerSocket, buffer);

                    if (IsConnected(playerSocket))
                    {
                        //Start listening to the message send by the user
                        playerSocket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, OnReceive, Tuple.Create(playerSocket, buffer));
                    }
                    else
                    {
                        playerSocket.Close();
                    }
                }
                catch (Exception ex)
                {
                    var state = (Tuple<Socket, byte[]>)ar.AsyncState;
                    var playerSocket = state.Item1;
                    var buffer = state.Item2;

                    playerSocket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, OnReceive, Tuple.Create(playerSocket, buffer));

                    RaiseExceptionThrown(ex);
                }
            }
        }

        private void OnSend(IAsyncResult ar)
        {
            try
            {
                ((Socket)ar.AsyncState).EndSend(ar);
            }
            catch (Exception ex)
            {
                RaiseExceptionThrown(ex);
            }
        }

        private static bool IsConnected(Socket socket)
        {
            try
            {
                return !socket.Poll(1, SelectMode.SelectRead) || socket.Available != 0;
            }
            catch (SocketException)
            {
                return false;
            }
        }

        protected override bool IsOnline()
        {
            return gameServerGatewaySocket != null;
        }
        #endregion
    }
}
