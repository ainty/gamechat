﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Definitions;
using GameServerPhysical;

namespace GameServerCore
{
    public class GameServer
    {
        #region Events

        public delegate void ExceptionThrownDelegate(Exception ex);

        public event EventHandler DataReceivedEvent;

        public event ExceptionThrownDelegate ExceptionThrownEvent;
        #endregion

        #region Fields

        private readonly IPhysicalServer physicalServer;
        private readonly Serialization<Package> serialization;
        private readonly ConcurrentDictionary<string, PlayerInfo> userCache;
        private readonly ConcurrentDictionary<string, GameInfo> gameCache;

        private readonly Action<string> raiseDataReceivedEvent;

        #endregion

        #region Properties

        public ICollection<string> Games => gameCache.Keys;

        public ICollection<string> GetPlayers(string game)
        {
            if (!string.IsNullOrEmpty(game) && gameCache.TryGetValue(game, out var cachedGame))
            {
                return cachedGame.Players.Keys;
            }

            var users = userCache.Values
                .Where(x => x.Games.IsEmpty)
                .Select(x => x.UserName)
                .ToList();

            return users;
        }

        #endregion

        #region .ctor

        public GameServer(IPhysicalServer physicalServer)
        {
            serialization = new Serialization<Package>();

            this.physicalServer = physicalServer ?? throw new ArgumentNullException(nameof(physicalServer));

            this.physicalServer.ExceptionThrownEvent += RaiseExceptionThrownEvent;

            this.physicalServer.BytesReceivedEvent += (playerSocket, data) => ProcessIncomingPackage(playerSocket, serialization.GetObject(data));

            userCache = new ConcurrentDictionary<string, PlayerInfo>();
            gameCache = new ConcurrentDictionary<string, GameInfo>();

            raiseDataReceivedEvent = x =>
            {
                try
                {
                    DataReceivedEvent?.Invoke(x, EventArgs.Empty);
                }
                catch (Exception ex)
                {
                    RaiseExceptionThrownEvent(ex);
                }
            };
        }

        #endregion

        #region Public Methods

        public void Start()
        {
            physicalServer.Start();
        }

        public void Stop()
        {
            physicalServer.Stop();
        }
        #endregion

        #region Player Registration Methods

        private void RegisterPlayer(object playerSocket, Package incomingPackage)
        {
            // TODO: double receive!
            if (incomingPackage.UserName != null)
            {
                userCache.TryAdd(incomingPackage.UserName, new PlayerInfo(incomingPackage.UserName, playerSocket));
            }
        }

        private void RegisterPlayerInGame(Package incomingPackage)
        {
            var user = incomingPackage.UserName;
            var game = incomingPackage.GameName;

            var info = userCache[user];
            info.Games.TryAdd(game, default);

            gameCache.TryAdd(game, new GameInfo
            {
                Players = new ConcurrentDictionary<string, string>()
            });

            gameCache[game].Players.TryAdd(user, default);
        }

        private void UnRegisterPlayerFromGame(Package incomingPackage)
        {
            var user = incomingPackage.UserName;
            var game = incomingPackage.GameName;

            if (userCache.TryGetValue(user, out var cachedUser))
            {
                cachedUser.Games.TryRemove(game, out _);
            }
            if (gameCache.TryGetValue(game, out var cachedGame))
            {
                cachedGame.Players.TryRemove(user, out _);
            }

            RemoveGameIfEmpty(game);
        }

        private void UnRegisterPlayer(Package incomingPackage)
        {
            userCache.TryRemove(incomingPackage.UserName, out _);
        }

        #endregion

        #region Pipeline Methods

        private void ProcessIncomingPackage(object playerSocket, Package incomingPackage)
        {
            // Perform incoming message
            var answeringPackage = GetAnswerPackage(incomingPackage);

            // Perform registration things
            switch (incomingPackage.Action)
            {
                case GameActionType.LogIn:
                    RegisterPlayer(playerSocket, incomingPackage);
                    break;
                case GameActionType.LogOut:
                    UnRegisterPlayer(incomingPackage);
                    break;
                case GameActionType.GameIn:
                    RegisterPlayerInGame(incomingPackage);
                    break;
                case GameActionType.GameOut:
                    UnRegisterPlayerFromGame(incomingPackage);
                    break;
                case GameActionType.Message:
                case GameActionType.PlayersList:
                case GameActionType.GamesList:
                case GameActionType.Null:
                    break;
                default:
                    throw new ApplicationException(Resources.UnknownCommandWarning);
            }

            // Send answer to all who need to sent
            var buffer = serialization.GetBytes(answeringPackage);
            // TODO: ?!? check that buffer is not changed after first send
            foreach (var clientInfo in GetRecipients(answeringPackage))
            {
                clientInfo.Send(physicalServer, buffer);
            }

            OnDataReceive(answeringPackage);
        }

        private string GetAnswerContent(Package incomingPackage)
        {
            string result = null;

            switch (incomingPackage.Action)
            {
                case GameActionType.LogIn:
                    //Set the text of the message that we will broadcast to all users
                    result = $"<<<{incomingPackage.UserName} has joined the server>>>";
                    break;
                case GameActionType.LogOut:
                    result = $"<<<{incomingPackage.UserName} has left the server>>>";
                    break;
                case GameActionType.GameIn:
                    //Set the text of the message that we will broadcast to all users
                    result = $"<<<{incomingPackage.UserName} has joined the game {incomingPackage.GameName}>>>";
                    break;
                case GameActionType.GameOut:
                    result = $"<<<{incomingPackage.UserName} has left the game {incomingPackage.GameName}>>>";
                    break;
                case GameActionType.Message:
                    //Set the text of the message that we will broadcast to all users
                    result = $"{incomingPackage.UserName}: {incomingPackage.Content} in {incomingPackage.GameName}";
                    break;
                case GameActionType.PlayersList:
                    result = "";
                    //Collect the names of the user in the chat game
                    var coPlayers = GetRawCoPlayers(incomingPackage.GameName);
                    foreach (var client in coPlayers)
                    {
                        //To keep things simple we use asterisk as the marker to separate the user names
                        result += client.UserName + "*";
                    }
                    break;
                case GameActionType.GamesList:
                    //Collect the names of the user in the chat game
                    foreach (var gameName in gameCache.Keys)
                    {
                        //To keep things simple we use asterisk as the marker to separate the user names
                        result += gameName + "*";
                    }
                    break;
                case GameActionType.Null:
                    break;
                default:
                    throw new ApplicationException(Resources.UnknownCommandWarning);
            }
            return result;
        }

        private Package GetAnswerPackage(Package incomingPackage)
        {
            var broadcastPackage = new Package
            {
                Action = incomingPackage.Action,
                UserName = incomingPackage.UserName,
                GameName = incomingPackage.GameName,
                Content = GetAnswerContent(incomingPackage)
            };

            return broadcastPackage;
        }

        private IEnumerable<PlayerInfo> GetRecipients(Package incomingPackage)
        {
            var result = new List<PlayerInfo>();

            switch (incomingPackage.Action)
            {
                case GameActionType.LogIn:
                    // possibly no one or all not in game
                    break;
                case GameActionType.LogOut:
                    // possibly no one or all not in game
                    break;
                case GameActionType.GameIn:
                case GameActionType.GameOut:
                    // all game partners except current
                    result.AddRange(GetRawCoPlayers(incomingPackage.GameName, incomingPackage.UserName));
                    break;
                case GameActionType.PlayersList:
                case GameActionType.GamesList:
                    // only current
                    result.Add(userCache[incomingPackage.UserName]);
                    break;
                case GameActionType.Message:
                    // all game partners
                    result.AddRange(GetRawCoPlayers(incomingPackage.GameName));
                    break;
                case GameActionType.Null:
                    break;
            }

            return result;
        }

        private IEnumerable<PlayerInfo> GetRawCoPlayers(string gameName, string userName = null)
        {
            var result = new List<PlayerInfo>();

            if (!string.IsNullOrEmpty(gameName) && gameCache.TryGetValue(gameName, out var game))
            {
                foreach (var playerName in game.Players)
                {
                    if (userCache.TryGetValue(playerName.Key, out var cachedUser))
                    {
                        result.Add(cachedUser);
                    }
                }

                var needExcludeParticularUser = !string.IsNullOrEmpty(userName);
                if (needExcludeParticularUser)
                {
                    result.RemoveAll(info => info.UserName == userName);
                }
            }

            return result;
        }

        private void RemoveGameIfEmpty(string gameName)
        {
            if (gameCache.TryGetValue(gameName, out var game) && !game.Players.Any())
            {
                gameCache.TryRemove(gameName, out _);
            }
        }
        #endregion

        #region Event Methods

        private void OnDataReceive(Package broadcastPackage)
        {
            // Raise data receive event
            if (DataReceivedEvent != null)
            {
                switch (broadcastPackage.Action)
                {
                    case GameActionType.LogIn:
                    case GameActionType.LogOut:
                    case GameActionType.GameIn:
                    case GameActionType.GameOut:
                    case GameActionType.Message:
                        raiseDataReceivedEvent(broadcastPackage.Content);
                        break;
                }
            }
        }

        private void OnDataReceivedEnd(IAsyncResult ar)
        {
            try
            {
                ((EventHandler)ar.AsyncState).EndInvoke(ar);
            }
            catch (Exception ex)
            {
                RaiseExceptionThrownEvent(ex);
            }
        }

        private void RaiseExceptionThrownEvent(Exception ex)
        {
            if (ExceptionThrownEvent != null)
            {
                ExceptionThrownEvent(ex);
            }
        }
        #endregion

        #region Nested Structures

        private sealed class PlayerInfo
        {
            internal readonly string UserName;
            internal readonly ConcurrentDictionary<string, string> Games;
            private readonly object socket;

            internal PlayerInfo(string username, object socket)
            {
                UserName = username;
                this.socket = socket;
                Games = new ConcurrentDictionary<string, string>();
            }

            internal void Send(IPhysicalServer physicalServer, byte[] bytes)
            {
                physicalServer.Send(socket, bytes);
            }
        }

        private sealed class GameInfo
        {
            public ConcurrentDictionary<string, string> Players;
        }

        #endregion
    }
}
