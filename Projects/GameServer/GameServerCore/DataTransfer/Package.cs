using System;

namespace GameServerCore
{
    /// <summary>
    /// The data structure by which the server and the client interact with 
    /// each other
    /// </summary>
    class Package
    {
        public Package()
        {
            Action = GameActionType.Null;
            Content = null;
            UserName = null;
            GameName = null;
        }

        public GameActionType Action;  
        public string UserName;      
        public string GameName;  
        public string Content;   
    }
}
