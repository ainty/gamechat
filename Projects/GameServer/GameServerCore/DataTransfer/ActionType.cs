namespace GameServerCore
{
    /// <summary>
    /// The commands for interaction between server and client
    /// </summary>
    enum GameActionType
    {
        /// <summary>
        /// Empty command
        /// </summary>
        Null = 0,

        /// <summary>
        /// Log into the server
        /// </summary>
        LogIn,

        /// <summary>
        /// Logout of the server
        /// </summary>
        LogOut,

        /// <summary>
        /// Join to game
        /// </summary>
        GameIn,

        /// <summary>
        /// Exit from game
        /// </summary>
        GameOut,

        /// <summary>
        /// Send message to all in specified game
        /// </summary>
        Message,

        /// <summary>
        /// Get user list of specified game
        /// </summary>
        PlayersList,

        /// <summary>
        /// Get game list
        /// </summary>
        GamesList
    }
}
