﻿using System;
using System.Collections.Concurrent;

namespace GameServerPhysical.Extensions
{
    public sealed class AdaptingPhysicalServer : IPhysicalServer
    {
        private readonly IPhysicalServer physicalServer;
        private readonly ConcurrentDictionary<Guid, object> guidSocketMap = new();
        private readonly ConcurrentDictionary<object, Guid> socketGuidMap = new();

        public event BytesReceivedDelegate BytesReceivedEvent = (o, e) => { };
        public event ExceptionThrownDelegate ExceptionThrownEvent = ex => { };

        public AdaptingPhysicalServer(IPhysicalServer physicalServer)
        {
            this.physicalServer = physicalServer ?? throw new ArgumentNullException(nameof(physicalServer));

            this.physicalServer.BytesReceivedEvent += AdaptingOnBytesReceived;
            this.physicalServer.ExceptionThrownEvent += RaiseExceptionThrownEvent;
        }

        public void Send(object socket, byte[] message)
        {
            try
            {
                var guid = (Guid)socket;

                var adaptedSocket = guidSocketMap[guid];

                physicalServer.Send(adaptedSocket, message);
            }
            catch (Exception ex)
            {
                ExceptionThrownEvent.Invoke(ex);
            }
        }

        public void Start()
        {
            physicalServer.Start();
        }

        public void Stop()
        {
            guidSocketMap.Clear();
            socketGuidMap.Clear();

            physicalServer.Stop();
        }

        private void AdaptingOnBytesReceived(object socket, byte[] data)
        {
            Guid guid;

            if (socketGuidMap.ContainsKey(socket))
            {
                guid = socketGuidMap[socket];
            }
            else
            {
                guid = Guid.NewGuid();

                socketGuidMap.TryAdd(socket, guid);
                guidSocketMap.TryAdd(guid, socket);
            }

            BytesReceivedEvent.Invoke(guid, data);
        }

        private void RaiseExceptionThrownEvent(Exception ex)
        {
            ExceptionThrownEvent.Invoke(ex);
        }
    }
}
