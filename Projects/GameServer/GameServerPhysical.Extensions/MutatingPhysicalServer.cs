﻿using System;

namespace GameServerPhysical.Extensions
{
    public sealed class MutatingPhysicalServer : IPhysicalServer
    {
        private readonly IPhysicalServer physicalServer;
        private readonly Mutator mutator;

        public event BytesReceivedDelegate BytesReceivedEvent = (o, e) => { };
        public event ExceptionThrownDelegate ExceptionThrownEvent = ex => { };

        public MutatingPhysicalServer(IPhysicalServer physicalServer, Mutator mutator = null)
        {
            this.physicalServer = physicalServer ?? throw new ArgumentNullException(nameof(physicalServer));
            this.mutator = mutator ?? new DefaultMutator();

            this.physicalServer.BytesReceivedEvent += OnBytesReceived;
            this.physicalServer.ExceptionThrownEvent += OnExceptionThrown;
        }

        public void Send(object socket, byte[] message)
        {
            var mutatedMessage = mutator.ForwardTransform(message);
            
            physicalServer.Send(socket, mutatedMessage);
        }

        public void Start()
        {
            physicalServer.Start();
        }

        public void Stop()
        {
            physicalServer.Stop();
        }

        private void OnBytesReceived(object socket, byte[] data)
        {
            var demutatedData = mutator.BackwardTransform(data);

            BytesReceivedEvent.Invoke(socket, demutatedData);
        }

        private void OnExceptionThrown(Exception ex)
        {
            ExceptionThrownEvent.Invoke(ex);
        }

        private class DefaultMutator : Mutator
        {
            public override byte[] ForwardTransform(byte[] bytes)
            {
                return bytes;
            }

            public override byte[] BackwardTransform(byte[] bytes)
            {
                return bytes;
            }
        }
    }
}
