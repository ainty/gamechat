﻿using System;
using System.Collections.Generic;

namespace GameServerPhysical.Extensions
{
    public sealed class CompositePhysicalServer : IPhysicalServer
    {
        private readonly IEnumerable<IPhysicalServer> physicalServers;

        public event BytesReceivedDelegate BytesReceivedEvent = (o, e) => { };
        public event ExceptionThrownDelegate ExceptionThrownEvent = ex => { };

        public CompositePhysicalServer(IEnumerable<IPhysicalServer> physicalServers)
        {
            if (physicalServers == null)
            {
                throw new ArgumentNullException(nameof(physicalServers));
            }

            this.physicalServers = new List<IPhysicalServer>(physicalServers);

            ConfigureEventsRouting();
        }

        public void Send(object socket, byte[] message)
        {
            try
            {
                var socketComposite = socket as ServerSocketPair;
                socketComposite?.Server.Send(socketComposite.Socket, message);
            }
            catch (Exception ex)
            {
                RaiseExceptionThrownEvent(ex);
            }
        }

        public void Start()
        {
            foreach (var physicalServer in physicalServers)
            {
                physicalServer.Start();
            }
        }

        public void Stop()
        {
            foreach (var physicalServer in physicalServers)
            {
                physicalServer.Stop();
            }
        }

        private void ConfigureEventsRouting()
        {
            foreach (var physicalServer in physicalServers)
            {
                var server = physicalServer;
                physicalServer.BytesReceivedEvent += (socket, data) => CompositeOnBytesReceived(server, socket, data);
                physicalServer.ExceptionThrownEvent += RaiseExceptionThrownEvent;
            }
        }

        private void CompositeOnBytesReceived(IPhysicalServer server, object socket, byte[] data)
        {
            var serverSocketPair = new ServerSocketPair { Server = server, Socket = socket };
            BytesReceivedEvent.Invoke(serverSocketPair, data);
        }

        private void RaiseExceptionThrownEvent(Exception ex)
        {
            ExceptionThrownEvent.Invoke(ex);
        }

        private class ServerSocketPair
        {
            public IPhysicalServer Server;

            public object Socket;
        }
    }
}
