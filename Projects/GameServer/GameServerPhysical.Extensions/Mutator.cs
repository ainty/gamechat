﻿namespace GameServerPhysical.Extensions
{
    public abstract class Mutator
    {
        public abstract byte[] ForwardTransform(byte[] bytes);

        public abstract byte[] BackwardTransform(byte[] bytes);
    }
}
