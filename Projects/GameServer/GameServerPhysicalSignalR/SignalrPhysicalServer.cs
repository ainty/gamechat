﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;
using ConnectionSchemes;
using Definitions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace GameServerPhysical
{
    [Way("tap"), Protocol("signalr")]
    public sealed class SignalRPhysicalServer : PhysicalServer
    {
        private readonly int _port;
        private readonly IPAddress _address;
        private IHost _host;
        private bool _isOnline;
        private ChatHub _hub;

        public SignalRPhysicalServer(SocketProvider socketProvider, EndPointProvider endPointProvider) : base(
            socketProvider, endPointProvider)
        {
            var ep = (endPointProvider.CreateEndpointForIncoming() as IPEndPoint);
            _port = ep?.Port ?? 5000;
            _address = ep?.Address ?? IPAddress.Any;
        }

        public override void Send(object connectionId, byte[] message)
        {
            var hub = _host.Services.GetRequiredService(typeof(ChatHub)) as ChatHub;

            hub?.Send(connectionId.ToString(), message);
        }

        public override void Start()
        {
            if (_isOnline) return;
            _isOnline = true;

            _hub = new ChatHub(this);

            _host = Host
                .CreateDefaultBuilder()
                .ConfigureLogging(builder =>
                {
                    builder.SetMinimumLevel(LogLevel.Warning);
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseUrls($"http://{_address}:{_port}");

                    webBuilder
                        .ConfigureServices(services =>
                        {
                            services.AddSignalR();
                            services.AddHostedService<Worker>();
                            services.TryAddSingleton(sp => _hub);
                        })
                        .Configure(app =>
                        {
                            app.UseRouting();
                            app.UseEndpoints(endpoints => { endpoints.MapHub<ChatHub>("/hubs/chat"); });
                        });
                })
                .Build();

            Task.Run(async () => { await _host.StartAsync(); });
        }

        public override void Stop()
        {
            if (!_isOnline) return;
            _isOnline = false;

            Task.Run(async () =>
            {
                await _host.StopAsync();
                _host.Dispose();
            });
        }

        protected override bool IsOnline() => _isOnline;

        public class ChatHub : Hub<IClock>
        {
            private readonly SignalRPhysicalServer _physicalServer;

            public ChatHub(SignalRPhysicalServer physicalServer)
            {
                _physicalServer = physicalServer;
            }

            public async Task RegisterMessage(byte[] message)
            {
                _physicalServer.RaiseBytesReceived(Context.ConnectionId, message);

                await Task.CompletedTask;
            }

            internal void Send(string connectionId, byte[] message)
            {
                Clients.Client(connectionId).PublishMessage(message);
            }
        }

        public class Worker : BackgroundService
        {
            private readonly IHubContext<ChatHub, IClock> _clockHub;

            public Worker(IHubContext<ChatHub, IClock> clockHub)
            {
                _clockHub = clockHub;
            }

            protected override async Task ExecuteAsync(CancellationToken stoppingToken)
            {
                while (!stoppingToken.IsCancellationRequested)
                {
                    //await _clockHub.Clients.All.PublishMessage(Encoding.UTF8.GetBytes("ping"));
                    await Task.Delay(1000, stoppingToken);
                }
            }
        }

        public interface IClock
        {
            Task PublishMessage(byte[] message);
            Task RegisterMessage(byte[] message);
        }
    }
}
