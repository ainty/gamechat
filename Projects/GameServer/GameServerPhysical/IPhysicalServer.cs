﻿using System;

namespace GameServerPhysical
{
    public delegate void BytesReceivedDelegate(object source, byte[] data);
    public delegate void ExceptionThrownDelegate(Exception ex);

    public interface IPhysicalServer
    {
        event BytesReceivedDelegate BytesReceivedEvent;
        event ExceptionThrownDelegate ExceptionThrownEvent;
        void Send(object target, byte[] message);
        void Start();
        void Stop();
    }
}
