﻿using System;
using ConnectionSchemes;

namespace GameServerPhysical
{
    public abstract class PhysicalServer : IPhysicalServer
    {
        protected const int BufferSize = 1024;
        protected readonly EndPointProvider EndPointProvider;
        protected readonly SocketProvider SocketProvider;

        protected PhysicalServer(SocketProvider socketProvider, EndPointProvider endPointProvider)
        {
            SocketProvider = socketProvider ?? throw new ArgumentNullException(nameof(socketProvider));
            EndPointProvider = endPointProvider ?? throw new ArgumentNullException(nameof(endPointProvider));
        }

        public event BytesReceivedDelegate BytesReceivedEvent = (o, e) => { };
        public event ExceptionThrownDelegate ExceptionThrownEvent = ex => { };

        public abstract void Send(object target, byte[] message);

        public abstract void Start();

        public abstract void Stop();

        protected abstract bool IsOnline();

        protected void RaiseBytesReceived(object source, byte[] data)
        {
            BytesReceivedEvent.Invoke(source, data);
        }

        protected void RaiseExceptionThrown(Exception ex)
        {
            ExceptionThrownEvent.Invoke(ex);
        }

        protected static ArgumentNullException UserSocketNullException(string message)
        {
            return new ArgumentNullException(message);
        }
    }
}
