using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using GameServerCore;

namespace GameServerUI
{
    public partial class GameServerUi : Form
    {
        private readonly GameServer gameServer;

        public GameServerUi(GameServer gameServer)
        {
            InitializeComponent();

            this.gameServer = gameServer;
            this.gameServer.DataReceivedEvent += GameServer_DataReceived;
            this.gameServer.ExceptionThrownEvent += WriteLog;
        }

        private void GameServerUI_Load(object sender, EventArgs e)
        {
            try
            {
                gameServer.Start();
            }
            catch (Exception ex)
            {
                WriteLog(ex);
            }
        }

        private void GameServerUI_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show(@"Are you sure you want to stop server?", @"Game Server",
              MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.No)
            {
                e.Cancel = true;
                return;
            }

            try
            {
                gameServer.Stop();
            }
            catch (Exception ex)
            {
                WriteLog(ex);
            }
        }

        private void GameServer_DataReceived(object sender, EventArgs e)
        {
            try
            {
                var v = string.Empty;

                if (txtLog.InvokeRequired)
                {
                    txtLog.Invoke((MethodInvoker)(() => v = txtLog.Text));

                    v += sender + Environment.NewLine;

                    txtLog.Invoke((MethodInvoker)(() => txtLog.Text = v));
                }
                else
                {
                    v = txtLog.Text;
                    v += sender + Environment.NewLine;
                    txtLog.Text = v;
                }
            }
            catch (Exception ex)
            {
                WriteLog(ex);
            }
        }

        private void WriteLog(Exception ex)
        {
            const string caption = "Game Server";
            var message = ex.Message + ex.StackTrace;
            MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btnLoadGames_Click(object sender, EventArgs e)
        {
            listBoxGamePlayers.SelectedIndex = -1;
            listBoxGamePlayers.Items.Clear();

            comboBoxGames.SelectedIndex = -1;
            comboBoxGames.Items.Clear();
            comboBoxGames.Items.Add("(not in game)");
            comboBoxGames.Items.AddRange(gameServer.Games.Cast<object>().ToArray());
        }

        private void comboBoxGames_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBoxGamePlayers.Items.Clear();

            var gameIndex = comboBoxGames.SelectedIndex;
            if (gameIndex >= 0)
            {
                var game = comboBoxGames.Items[gameIndex].ToString();
                
                var users = game == "(not in game)"
                    ? gameServer.GetPlayers(null)
                    : gameServer.GetPlayers(game);

                listBoxGamePlayers.Items.AddRange(users.Cast<object>().ToArray());
            }
        }
    }
}