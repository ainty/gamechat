using System;
using System.Net;
using System.Windows.Forms;
using ConnectionSchemes;
using GameServerCore;
using GameServerPhysical;
using Definitions;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using GameServerPhysical.Extensions;
using SocketProviders;

namespace GameServerUI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            TaskScheduler.UnobservedTaskException += TaskScheduler_UnobservedTaskException;
            Application.ThreadException += Application_ThreadException;

            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(ComposeGameServerUserInterface());
        }

        private static GameServerUi ComposeGameServerUserInterface()
        {
            var doc = GetXmlConfig();

            var physicalServer = ComposeHierarchically(doc.Root);

            return new GameServerUi(new GameServer(physicalServer));
        }

        private static IPhysicalServer ComposePhysicalServer(IPAddress address, int port, string protocol, string way)
        {
            var endPointScheme = new EndPointScheme(address, port);
            var socketScheme = new SocketScheme(endPointScheme);

            var byProtocol = TypeHelper.Combine<Type>(
               TypeHelper.Has<ProtocolAttribute>,
               type => TypeHelper.Get<ProtocolAttribute>(type).Protocol == protocol);

            var filter = TypeHelper.Combine((Type t) => true);
            var endPointProvider = TypeHelper.Get<EndPointProvider>(filter, endPointScheme);

            filter = TypeHelper.Combine(byProtocol, TypeHelper.Is<SocketProvider>());
            var socketProvider = TypeHelper.Get<SocketProvider>(filter, socketScheme);

            filter = TypeHelper.Combine(byProtocol, TypeHelper.With<WayAttribute>(way));
            var physicalServer = TypeHelper.Get<IPhysicalServer>(filter, socketProvider, endPointProvider);

            return physicalServer;
        }

        private static IPhysicalServer ComposePhysicalServer(IEnumerable<IPhysicalServer> physicalServers)
        {
            return new AdaptingPhysicalServer(new CompositePhysicalServer(physicalServers));
        }

        private static IPhysicalServer ComposeHierarchically(XElement element)
        {
            if (element.Attributes().Any())
            {
                var address = IPAddress.Parse(element.Attribute("ip").Value);
                var port = int.Parse(element.Attribute("port").Value);
                var protocol = element.Attribute("protocol").Value;
                var way = element.Attribute("way").Value;

                return ComposePhysicalServer(address, port, protocol, way);
            }

            var physicalServers = element.Elements("server").Select(ComposeHierarchically).ToList();

            return ComposePhysicalServer(physicalServers);
        }

        private static XDocument GetXmlConfig()
        {
            using (Stream stream = typeof(Program).Assembly.GetManifestResourceStream("GameServerUI.Resources.servers.xml"))
            {
                return XDocument.Load(stream);
            }
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            WriteLog(e.ExceptionObject as Exception, 1);
        }

        private static void TaskScheduler_UnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            WriteLog(e.Exception, 2);
        }

        private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            WriteLog(e.Exception, 3);
        }

        private static void WriteLog(Exception ex, int index)
        {
            using (var writer = new StreamWriter(@"D:\server" + index.ToString() + ".log"))
            {
                writer.Write(ex.StackTrace);
                writer.Flush();
            }
        }
    }
}