using System.ComponentModel;
using System.Windows.Forms;

namespace GameServerUI
{
    partial class GameServerUi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtLog = new System.Windows.Forms.TextBox();
            this.comboBoxGames = new System.Windows.Forms.ComboBox();
            this.btnLoadGames = new System.Windows.Forms.Button();
            this.listBoxGamePlayers = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // txtLog
            // 
            this.txtLog.BackColor = System.Drawing.SystemColors.Window;
            this.txtLog.Location = new System.Drawing.Point(15, 14);
            this.txtLog.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = true;
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLog.Size = new System.Drawing.Size(311, 279);
            this.txtLog.TabIndex = 0;
            // 
            // comboBoxGames
            // 
            this.comboBoxGames.FormattingEnabled = true;
            this.comboBoxGames.Location = new System.Drawing.Point(459, 12);
            this.comboBoxGames.Name = "comboBoxGames";
            this.comboBoxGames.Size = new System.Drawing.Size(224, 23);
            this.comboBoxGames.TabIndex = 1;
            this.comboBoxGames.SelectedIndexChanged += new System.EventHandler(this.comboBoxGames_SelectedIndexChanged);
            // 
            // btnLoadGames
            // 
            this.btnLoadGames.Location = new System.Drawing.Point(333, 12);
            this.btnLoadGames.Name = "btnLoadGames";
            this.btnLoadGames.Size = new System.Drawing.Size(120, 23);
            this.btnLoadGames.TabIndex = 3;
            this.btnLoadGames.Text = "Load Games";
            this.btnLoadGames.UseVisualStyleBackColor = true;
            this.btnLoadGames.Click += new System.EventHandler(this.btnLoadGames_Click);
            // 
            // listBoxGamePlayers
            // 
            this.listBoxGamePlayers.FormattingEnabled = true;
            this.listBoxGamePlayers.ItemHeight = 15;
            this.listBoxGamePlayers.Location = new System.Drawing.Point(459, 41);
            this.listBoxGamePlayers.Name = "listBoxGamePlayers";
            this.listBoxGamePlayers.Size = new System.Drawing.Size(224, 244);
            this.listBoxGamePlayers.TabIndex = 4;
            // 
            // GameServerUi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(695, 307);
            this.Controls.Add(this.listBoxGamePlayers);
            this.Controls.Add(this.btnLoadGames);
            this.Controls.Add(this.comboBoxGames);
            this.Controls.Add(this.txtLog);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.MaximizeBox = false;
            this.Name = "GameServerUi";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "GameServer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GameServerUI_FormClosing);
            this.Load += new System.EventHandler(this.GameServerUI_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TextBox txtLog;
        private ComboBox comboBoxGames;
        private Button btnLoadGames;
        private ListBox listBoxGamePlayers;
    }
}

