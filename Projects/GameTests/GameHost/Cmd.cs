﻿namespace GameHost
{
    public abstract class Cmd
    {
        public static readonly Connect ConnectCmd = new Connect();
        public static readonly Disconnect DisconnectCmd = new Disconnect();
        public static readonly Leave LeaveCmd = new Leave();
        public static readonly Ping PingCmd = new Ping();

        public class Msg : Cmd
        {
            internal Msg(string message)
            {
                Text = message;
            }

            public string Text { get; }
        }

        public class Join : Cmd
        {
            internal Join(string game)
            {
                Game = game;
            }

            public string Game { get; }
        }

        public class Connect : Cmd
        {
            internal Connect()
            {
            }
        }

        public class Disconnect : Cmd
        {
            internal Disconnect()
            {
            }
        }

        public class Leave : Cmd
        {
            internal Leave()
            {
            }
        }

        public class Ping : Cmd
        {
            internal Ping()
            {
            }
        }

        public static Msg Message(string msg) => new Msg(msg);
        public static Join JoinGame(string game) => new Join(game);
    }
}