﻿using System;
using System.Collections.Concurrent;
using System.Runtime.CompilerServices;
using System.Threading;

namespace GameHost
{
    public sealed class EventAwaiter<T> : INotifyCompletion
    {
        private readonly ConcurrentQueue<T> _events = new ConcurrentQueue<T>();
        private Action _continuation;

        public EventAwaiter<T> GetAwaiter() => this;

        // Used implicitly
        // ReSharper disable once UnusedMember.Global
        public bool IsCompleted => _events.Count > 0;

        // Used implicitly
        // ReSharper disable once UnusedMember.Global
        public void OnCompleted(Action continuation)
        {
            Volatile.Write(ref _continuation, continuation);
        }

        // Used implicitly
        // ReSharper disable once UnusedMember.Global
        public T GetResult()
        {
            _events.TryDequeue(out var eventArgs);
            return eventArgs;
        }

        public void EventRaised(object sender, T @event)
        {
            _events.Enqueue(@event);
            var continuation = Interlocked.Exchange(ref _continuation, null);
            continuation?.Invoke();
        }

        public void EventRaised(T @event)
        {
            EventRaised(null, @event);
        }
    }
}
