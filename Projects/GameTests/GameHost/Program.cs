﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using ConnectionSchemes;
using GameClientCore;
using GameClientPhysical;
using GameServerCore;
using GameServerPhysical;
using GameServerPhysical.Extensions;
using SocketProviders;

namespace GameHost
{
    public class Program
    {
        private static readonly Dictionary<string, EndPointScheme> Eps = new()
        {
            {"tcp-tap", new EndPointScheme(IPAddress.Loopback, 10001)},
            {"tcp-eap", new EndPointScheme(IPAddress.Loopback, 10002)},
            {"tcp-apm", new EndPointScheme(IPAddress.Loopback, 10003)},
            {"udp-tap", new EndPointScheme(IPAddress.Loopback, 10004)},
            {"udp-apm", new EndPointScheme(IPAddress.Loopback, 10005)},
            {"signalr", new EndPointScheme(IPAddress.Loopback, 10006)}
        };

        public static async Task Main()
        {
            using (var serverCts = new CancellationTokenSource())
            using (var clientCts = new CancellationTokenSource())
            {
                var eventAwaiter = new EventAwaiter<string>();
                _ = ShowEvents(eventAwaiter, serverCts.Token);

                var serverTask = BuildServer(eventAwaiter, BuildPhysicalServer(), serverCts.Token);

                var (client1, client1Task) = BuildClient("p1", eventAwaiter, BuildPhysicalClient("tcp-tap"), clientCts.Token);
                var (client2, client2Task) = BuildClient("p2", eventAwaiter, BuildPhysicalClient("tcp-eap"), clientCts.Token);
                var (client3, client3Task) = BuildClient("p3", eventAwaiter, BuildPhysicalClient("tcp-apm"), clientCts.Token);
                var (client4, client4Task) = BuildClient("p4", eventAwaiter, BuildPhysicalClient("udp-tap"), clientCts.Token);
                var (client5, client5Task) = BuildClient("p5", eventAwaiter, BuildPhysicalClient("udp-apm"), clientCts.Token);
                var (client6, client6Task) = BuildClient("p6", eventAwaiter, BuildPhysicalClient("signalr"), clientCts.Token);

                while (true)
                {
                    Console.WriteLine("Waiting for input...");
                    var cmd = Console.ReadLine();
                    if ((cmd ?? string.Empty).Equals("y", StringComparison.InvariantCultureIgnoreCase))
                    {
                        break;
                    }

                    switch (cmd)
                    {
                        case "run":
                            ConnectClients();

                            Script();

                            LeaveClients();

                            DisconnectClients();
                            break;

                        case "connect":
                            ConnectClients();
                            break;

                        case "script":
                            Script();
                            break;

                        case "leave":
                            LeaveClients();
                            break;

                        case "disconnect":
                            DisconnectClients();
                            break;
                    }
                }

                Console.WriteLine("Host stopping...");

                clientCts.Cancel();
                await Task.WhenAll(client1Task, client2Task, client3Task, client4Task, client5Task, client6Task);

                serverCts.Cancel();
                await serverTask;
                await Task.Delay(1000, CancellationToken.None);

                Console.WriteLine("Host stopped");

                void ConnectClients()
                {
                    client1.EventRaised(Cmd.ConnectCmd);
                    client2.EventRaised(Cmd.ConnectCmd);
                    client3.EventRaised(Cmd.ConnectCmd);
                    client4.EventRaised(Cmd.ConnectCmd);
                    client5.EventRaised(Cmd.ConnectCmd);
                    client6.EventRaised(Cmd.ConnectCmd);
                }

                void Script()
                {
                    client1.EventRaised(Cmd.JoinGame("gm1"));
                    client2.EventRaised(Cmd.JoinGame("gm1"));
                    client3.EventRaised(Cmd.JoinGame("gm2"));
                    client4.EventRaised(Cmd.JoinGame("gm2"));

                    client5.EventRaised(Cmd.JoinGame("gm3"));
                    client6.EventRaised(Cmd.JoinGame("gm3"));

                    client1.EventRaised(Cmd.Message("hi 2!"));
                    client2.EventRaised(Cmd.Message("hi 1!"));

                    client3.EventRaised(Cmd.Message("hi 4!"));
                    client4.EventRaised(Cmd.Message("hi 3!"));

                    client5.EventRaised(Cmd.Message("hi ?"));
                    client6.EventRaised(Cmd.Message("hi ????"));

                    client2.EventRaised(Cmd.LeaveCmd);
                    client1.EventRaised(Cmd.Message("oops"));

                    client2.EventRaised(Cmd.JoinGame("gm2"));

                    client3.EventRaised(Cmd.Message("hi 2!"));
                    client4.EventRaised(Cmd.Message("hi 2!"));
                    client2.EventRaised(Cmd.Message("hi 3 and 4!"));
                }

                void LeaveClients()
                {
                    client1.EventRaised(Cmd.LeaveCmd);
                    client2.EventRaised(Cmd.LeaveCmd);
                    client3.EventRaised(Cmd.LeaveCmd);
                    client4.EventRaised(Cmd.LeaveCmd);
                    client5.EventRaised(Cmd.LeaveCmd);
                    client6.EventRaised(Cmd.LeaveCmd);
                }

                void DisconnectClients()
                {
                    client1.EventRaised(Cmd.DisconnectCmd);
                    client2.EventRaised(Cmd.DisconnectCmd);
                    client3.EventRaised(Cmd.DisconnectCmd);
                    client4.EventRaised(Cmd.DisconnectCmd);
                    client5.EventRaised(Cmd.DisconnectCmd);
                    client6.EventRaised(Cmd.DisconnectCmd);
                }
            }

            Console.WriteLine("Press any key to exit!");
            Console.ReadKey();
        }

        private static Task BuildServer(EventAwaiter<string> eventAwaiter, IPhysicalServer physicalServer, CancellationToken token)
        {
            return Task.Run(async () =>
            {
                Console.WriteLine("Server starting...");

                var server = new GameServer(physicalServer);

                server.DataReceivedEvent += (sender, args) => eventAwaiter.EventRaised($"[server] {sender}");
                server.ExceptionThrownEvent += exception => eventAwaiter.EventRaised($"[server-exception] {exception.Message} {exception.StackTrace}");

                server.Start();
                Console.WriteLine("Server started");

                while (!token.IsCancellationRequested)
                {
                    await Task.Delay(1000, CancellationToken.None);
                }

                Console.WriteLine("Server stopping...");
                server.Stop();
                Console.WriteLine("Server stopped");

            }, CancellationToken.None);
        }

        private static (EventAwaiter<Cmd>, Task) BuildClient(string userName, EventAwaiter<string> eventAwaiter, IPhysicalClient physicalClient, CancellationToken token)
        {
            var clientCmdAwaiter = new EventAwaiter<Cmd>();

            Task.Run(async () =>
            {
                while (clientCmdAwaiter != null)
                {
                    await Task.Delay(1000, CancellationToken.None);
                    clientCmdAwaiter?.EventRaised(Cmd.PingCmd);
                }
            }, CancellationToken.None);

            var task = Task.Run(async () =>
            {
                Console.WriteLine($"Client {userName} starting...");

                var client = new GameClient(physicalClient)
                {
                    UserName = userName
                };

                client.MessageEvent += msg => eventAwaiter.EventRaised($"[c-{client.UserName}(msg)] {msg}");
                client.LoginEvent += message => eventAwaiter.EventRaised($"c-{client.UserName}(connect) {message}");
                client.LogoutEvent += message => eventAwaiter.EventRaised($"c-{client.UserName}(disconnect) {message}");
                client.GameInEvent += message => eventAwaiter.EventRaised($"c-{client.UserName}(game-in) {message}");
                client.GameOutEvent += message => eventAwaiter.EventRaised($"c-{client.UserName}(game-out) {message}");
                client.GameListEvent += message => eventAwaiter.EventRaised($"c-{client.UserName}(games) {message}");
                client.PlayerListEvent += message => eventAwaiter.EventRaised($"c-{client.UserName}(players) {message}");
                client.LogEvent += (message, title) => eventAwaiter.EventRaised($"c-{client.UserName}(log) [{title}] {message}");

                Console.WriteLine($"Client {client.UserName} started");

                while (!token.IsCancellationRequested)
                {
                    var cmd = await clientCmdAwaiter;
                    switch (cmd)
                    {
                        case Cmd.Msg msg:
                            client.Send(msg.Text);
                            await Task.Delay(100, CancellationToken.None);
                            break;
                        case Cmd.Join join:
                            client.JoinToGame(join.Game);
                            await Task.Delay(2000, CancellationToken.None);
                            break;
                        case Cmd.Leave _:
                            client.LeaveGame();
                            await Task.Delay(2000, CancellationToken.None);
                            break;
                        case Cmd.Connect _:
                            client.Connect();
                            await Task.Delay(2000, CancellationToken.None);
                            break;
                        case Cmd.Disconnect _:
                            client.Disconnect();
                            await Task.Delay(2000, CancellationToken.None);
                            break;
                    }
                }

                clientCmdAwaiter = null;

                Console.WriteLine($"Client {client.UserName} stopping...");
                try
                {
                    client.Disconnect();
                }
                catch (ObjectDisposedException)
                {
                }
                Console.WriteLine($"Client {client.UserName} stopped");
            }, token);

            return (clientCmdAwaiter, task.ContinueWith(x => { }, CancellationToken.None));
        }

        private static async Task ShowEvents<T>(EventAwaiter<T> eventAwaiter, CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                Console.WriteLine(await eventAwaiter);
            }
        }

        private static IPhysicalServer BuildPhysicalServer()
        {
            var tcpPhysicalServers = new CompositePhysicalServer(
                new List<IPhysicalServer>
                {
                    new TcpTapPhysicalServer(
                        new TcpSocketProvider(new SocketScheme(Eps["tcp-tap"])),
                        new IpEndPointProvider(Eps["tcp-tap"])),
                    new TcpEapPhysicalServer(
                        new TcpSocketProvider(new SocketScheme(Eps["tcp-eap"])),
                        new IpEndPointProvider(Eps["tcp-eap"])),
                    new TcpApmPhysicalServer(
                        new TcpSocketProvider(new SocketScheme(Eps["tcp-apm"])),
                        new IpEndPointProvider(Eps["tcp-apm"]))
                });

            var udpPhysicalServers = new CompositePhysicalServer(
                new List<IPhysicalServer>
                {
                    new UdpTapPhysicalServer(
                        new UdpSocketProvider(new SocketScheme(Eps["udp-tap"])),
                        new IpEndPointProvider(Eps["udp-tap"])),
                    new UdpApmPhysicalServer(
                        new UdpSocketProvider(new SocketScheme(Eps["udp-apm"])),
                        new IpEndPointProvider(Eps["udp-apm"]))
                });

            var signalrPhysicalServer = new SignalRPhysicalServer(
                new SignalrSocketProvider(new SocketScheme(Eps["signalr"])),
                new IpEndPointProvider(Eps["signalr"]));

            var physicalServers = new IPhysicalServer[] { tcpPhysicalServers, udpPhysicalServers, signalrPhysicalServer };

            return new AdaptingPhysicalServer(new CompositePhysicalServer(physicalServers));
        }

        private static IPhysicalClient BuildPhysicalClient(string type)
        {
            var eps = Eps[type];

            return type.ToLower() switch
            {
                "tcp-tap" => new TcpTapPhysicalClient(
                    new TcpSocketProvider(new SocketScheme(eps)),
                    new IpEndPointProvider(eps)),
                "tcp-eap" => new TcpEapPhysicalClient(
                    new TcpSocketProvider(new SocketScheme(eps)),
                    new IpEndPointProvider(eps)),
                "tcp-apm" => new TcpApmPhysicalClient(
                    new TcpSocketProvider(new SocketScheme(eps)),
                    new IpEndPointProvider(eps)),
                "udp-tap" => new UdpTapPhysicalClient(
                    new UdpSocketProvider(new SocketScheme(eps)),
                    new IpEndPointProvider(eps)),
                "udp-apm" => new UdpApmPhysicalClient(
                    new UdpSocketProvider(new SocketScheme(eps)),
                    new IpEndPointProvider(eps)),
                "signalr" => new SignalRPhysicalClient(
                    new SignalrSocketProvider(new SocketScheme(eps)),
                    new IpEndPointProvider(eps)),
                _ => throw new ArgumentOutOfRangeException(type)
            };
        }
    }
}
