﻿using Definitions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameUnitTests
{
    [TestClass]
    public class SerializationUnitTests
    {
        [TestMethod]
        public void PackUnpackTest()
        {
            // Arrange
            var message = new Message
            {
                Command = Command.Cmd3,
                Username = "player",
                Chatroom = "room",
                Content = "Yes"
            };

            var serializer = new Serialization<Message>();

            // Act
            var unpacked = serializer.GetObject(serializer.GetBytes(message));

            // Assert
            Assert.AreEqual(message.Command, unpacked.Command);
            Assert.AreEqual(message.Username, unpacked.Username);
            Assert.AreEqual(message.Chatroom, unpacked.Chatroom);
            Assert.AreEqual(message.Content, unpacked.Content);
        }

        private enum Command
        {
            Cmd1,
            Cmd2,
            Cmd3,
            Cmd4
        }

        private class Message
        {
            public Command Command;
            public string Username;
            public string Chatroom;
            public string Content;
        }
    }
}
