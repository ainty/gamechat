﻿using System;
using ConnectionSchemes;
using GameClientPhysical;
using GameTests.Communication;

namespace GameUnitTests.Mocks
{
    class FakePhysicalClient : PhysicalClient
    {
        private readonly Communicator communicator;
        private readonly Guid clientGuid;

        public FakePhysicalClient(SocketProvider socketProvider, EndPointProvider gatewayEndPointProvider, Communicator communicator) : base(socketProvider, gatewayEndPointProvider)
        {
            this.communicator = communicator;
            clientGuid = Guid.NewGuid();

            BytesReceived += OnBytesReceived;
        }

        private void OnBytesReceived(byte[] data)
        {
            communicator.ClientSideReceived(clientGuid, data);
        }

        public override void Connect()
        {
            communicator.ClientConnect(clientGuid);
        }

        public override void Send(byte[] data, Action<string> callback = null)
        {
            communicator.ClientSideSend(clientGuid, data);

            if (callback != null)
            {
                callback(null);
            }
        }

        public override void Disconnect()
        {
            communicator.ClientDisconnect(clientGuid);
        }
    }
}
