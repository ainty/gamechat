﻿using ConnectionSchemes;
using GameServerPhysical;
using GameTests.Communication;

namespace GameUnitTests.Mocks
{
    class FakePhysicalServer : PhysicalServer
    {
        private readonly Communicator communicator;

        public FakePhysicalServer(SocketProvider socketProvider, EndPointProvider endPointProvider,
            Communicator communicator) : base(socketProvider, endPointProvider)
        {
            this.communicator = communicator;

            BytesReceivedEvent += communicator.ServerSideReceived;
        }

        public override void Send(object target, byte[] message)
        {
            communicator.ServerSideSend(target, message);
        }

        public override void Start()
        {
            communicator.ServerStart();
        }

        public override void Stop()
        {
            communicator.ServerStop();
        }

        protected override bool IsOnline()
        {
            return true;
        }
    }
}
