using System;
using System.Collections.Generic;
using System.Linq;

namespace GameTests.Communication
{
    internal class Communicator
    {
        private readonly Guid server = Guid.Empty;
        private readonly byte[] serverStart = new byte[1];
        private readonly byte[] serverStop = new byte[2];
        private readonly byte[] clientConnect = new byte[10];
        private readonly byte[] clientDisconnect = new byte[11];
        private readonly List<CommunicationEvent> events;

        public Communicator()
        {
            events = new List<CommunicationEvent>();
        }

        public IEnumerable<CommunicationEvent> GetEvents()
        {
            return events.ToArray();
        }

        public void ServerSideSend(object socket, byte[] message)
        {
            var guid = (Guid) socket;
            events.Add(new CommunicationEvent(Side.Server, Direction.Outgoing, message, guid));
        }

        public void ServerSideReceived(object socket, byte[] data)
        {
            var guid = (Guid) socket;
            events.Add(new CommunicationEvent(Side.Server, Direction.Incoming, data, guid));
        }

        public void ServerStart()
        {
            events.Add(new CommunicationEvent(Side.Server,Direction.Undefine, serverStart, server));
        }

        public void ServerStop()
        {
            events.Add(new CommunicationEvent(Side.Server,Direction.Undefine, serverStop, server));
        }

        public void ClientConnect(Guid clientGuid)
        {
            events.Add(new CommunicationEvent(Side.Client,Direction.Undefine, clientConnect, clientGuid));
        }

        public void ClientSideSend(Guid clientGuid, byte[] data)
        {
            events.Add(new CommunicationEvent(Side.Client, Direction.Outgoing, data, clientGuid));
        }

        public void ClientSideReceived(Guid clientGuid, byte[] data)
        {
            events.Add(new CommunicationEvent(Side.Client,Direction.Incoming, data, clientGuid));
        }

        public void ClientDisconnect(Guid clientGuid)
        {
            events.Add(new CommunicationEvent(Side.Client, Direction.Undefine, clientDisconnect, clientGuid));
        }
    }
}