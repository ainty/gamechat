﻿using System;
using System.Diagnostics;

namespace GameTests.Communication
{
    [DebuggerDisplay("{Side} {Direction} {Bytes} {ClientGuid}")]
    struct CommunicationEvent
    {
        private static readonly Guid ServerGuid = Guid.Empty;

        public readonly Side Side;
        public readonly Direction Direction;
        public readonly byte[] Bytes;
        public readonly Guid ClientGuid;

        public CommunicationEvent(Side side, Direction direction, byte[] bytes, Guid clientGuid)
        {
            Side = side;
            Direction = direction;
            Bytes = bytes ?? new byte[0];
            ClientGuid = side == Side.Client ? clientGuid : ServerGuid;
        }
    }
}
