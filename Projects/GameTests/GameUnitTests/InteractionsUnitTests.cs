﻿using System.Linq;
using ConnectionSchemes;
using GameClientCore;
using GameServerCore;
using GameServerPhysical;
using GameTests.Communication;
using GameUnitTests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocketProviders;

namespace GameUnitTests
{
    [TestClass]
    public class InteractionsUnitTests
    {
        private Communicator communicator;
        private GameServer gameServer;
        private IPhysicalServer physicalServer;

        [TestInitialize]
        public void Initialization()
        {
            communicator = new Communicator();

            physicalServer = new FakePhysicalServer(new CustomSocketProvider(new SocketScheme()), new IpEndPointProvider(new EndPointScheme()), communicator);
            gameServer = new GameServer(physicalServer);
        }

        [TestMethod]
        public void SingleClientHandShakeTest()
        {
            var physicalClient = new FakePhysicalClient(new CustomSocketProvider(new SocketScheme()), new IpEndPointProvider(new EndPointScheme()), communicator);
            var gameClient = new GameClient(physicalClient);

            gameServer.Start();
            gameClient.Connect();
            gameClient.Disconnect();
            gameServer.Stop();

            var events = communicator.GetEvents().ToList();

            Assert.IsTrue(events.Count == 5, "events.Count() != 5");
        }
    }
}
