This is a training project for asynchronous programming models based on following materials:

1. APM https://msdn.microsoft.com/en-us/library/ms228963(v=vs.110).aspx
2. EAP https://msdn.microsoft.com/en-us/library/ms228969(v=vs.110).aspx
3. TAP https://msdn.microsoft.com/en-us/library/hh873175(v=vs.110).aspx

Both of server side and client side are splitted into three layers:

1. User Interface         (very basic implemetation)
2. High Level Interaction (simple chatting logic) 
3. Network Interaction    (key point of interest in this project)

The Network Interaction layer consists of implementations of asynchronous programming patterns working over TCP, UDP and SignalR.
APM and EAP implementations are built with .NET 2.0.
TAP implementations are built with .NET 4.5.*.